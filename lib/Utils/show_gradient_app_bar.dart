import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class ShowGradientAppBar {
  /// ## Title
  ///
  final String title;

  /// ## Leading Icon
  /// Icon which displays at start or left side of the App bar.
  final IconData leadingIcon;

  /// ## Leading Icon Click Functionality
  ///
  /// You can use this to do some specific functionality.
  ///
  /// By-default ```
  /// Navigator.pop(context)
  /// ``` will be used
  final Function leadIconClickFuncitonality;
 
  /// ## Actions
  ///
  /// List of icons to display at end or right side of the App bar.
  final List<Widget> actions;

  ShowGradientAppBar(
      {@required this.title,
      @required this.leadingIcon,
      @required this.actions,
      @required this.leadIconClickFuncitonality});

  GradientAppBar show(BuildContext context) {
    return GradientAppBar(
      elevation: 0,
      gradient: LinearGradient(colors: [Color(0xff53d8d5), Color(0xff52a9e9)]),
      centerTitle: true,
      title: Padding(
        padding: const EdgeInsets.only(top: 4),
        child: Text(
          title,
          style: TextStyle(fontSize: 18),
        ),
      ),
      actions: actions,
      leading: Material(
        color: Colors.transparent,
        child: IconButton(
          icon: Icon(leadingIcon),
          onPressed: () {
            leadIconClickFuncitonality == null
                ? Navigator.pop(context)
                : leadIconClickFuncitonality();
          },
        ),
      ),
    );
  }
}
