class Consts {
  Consts._();

  static const double padding = 8.0;
  static const double avatarRadius = 50.0;

  static const int imageAcceptedSize = 1000000;
  static const String secureTokenKey = 'secure-storage-9823b-dfbh-45948';
  static const String secureCookie = 'secure-storage-55sd52-sdew-5d8v2z';
  static const String securePassKey = 'secure-storage-96dde2-63re2t-02ji3t';
  static const String secureUserKey = 'secure-storage-78oma1-40iaku6-00rmanag6';

  //static const String baseUrl = 'https://167.71.58.236/api/v1';
  static const String baseUrl = 'https://energysence.com/api/v1';

  //PATHS

  static const String loginUserPath = '/login/';
  static const String logoutUserPath = '/logout/';
  static const String facilityPath = '/facility/';
  static const String equipmentPath = '/equipment/';
  static const String facilityTypePath = '/facility_type/';
  static const String equipmentTypePath = '/equipment_type/';

// // SCREENS NAMES
// static const String facilityScreen = '/facility_screen';
// static const String mainFacilityScreen = '/main_facility_screen';
}
