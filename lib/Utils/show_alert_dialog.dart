import 'package:flutter/material.dart';
import 'package:sustain_energy/CustomViews/alert_dialog.dart';
import 'package:sustain_energy/CustomViews/alert_dialog_for_add_equiment.dart';
import 'package:sustain_energy/Utils/constant.dart';

class ShowAlertDialog {
  String title;
  String message;
  DialogTypes type;

  /// ## Positive functionality,
  ///
  /// if Dialog type is [DialogTypes.WITH_CAMERA_GALLERY] then use this
  /// for camera functionality
  ///
  /// Use like this
  /// ``` dart
  /// () { positiveButtonFunctionality(arg1, arg2, ... ); },
  /// ```

  Function positiveFunctionality;

  /// ## Negative functionality,
  ///
  /// If Dialog type is WITH_CAMERA_GALLERY then use this
  /// for Gallery functionality, send null if type is WITH_ONLY_POSITIVE_BUTTON or WITH_POSITIVE_NEGATIVE
  ///
  /// By Default it will dismiss the dialog box

  Function negativeFunctionality;

  /// True by default;
  ///
  /// Dismiss dialog when touch outside.
  bool isDismissable;

  /// Label for positive button,
  ///
  /// send null if type is WITH_CAMERA_GALLERY otherwise non-null
  String positiveButtonLabel;

  /// Label for negative button,
  ///
  /// Send null if type is WITH_CAMERA_GALLERY or WITH_ONLY_POSITIVE_BUTTON
  /// otherwise non-null,
  ///
  /// Negative button will by default dismiss the dialog box.
  String negativeButtonLabel;

  ShowAlertDialog(
      {@required this.title,
      @required this.message,
      @required this.isDismissable,
      @required this.negativeFunctionality,
      @required this.negativeButtonLabel,
      @required this.positiveFunctionality,
      @required this.positiveButtonLabel,
      @required this.type});

  void show(BuildContext context) {
    showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          final curvedValue = Curves.easeInOut.transform(a1.value);
          return Transform.scale(
            scale: curvedValue,
            child: Opacity(
                opacity: a1.value,
                child: type == DialogTypes.WITH_CAMERA_GALLERY
                    ? CustomAlertDialogForAE(
                        title: title,
                        content: message,
                        cameraFunctionality: () {
                          positiveFunctionality();
                        },
                        galleryFunctionality: () {
                          negativeFunctionality();
                        },
                      )
                    : type == DialogTypes.WITH_ONLY_POSITIVE_BUTTON
                        ? CustomAlertDialog(
                            title: title,
                            content: message,
                            positiveButtonLabel: positiveButtonLabel,
                            negativeButtonLabel: null,
                            positiveFunctionality: () =>
                                positiveFunctionality(),
                          )
                        : CustomAlertDialog(
                            title: title,
                            content: message,
                            positiveButtonLabel: positiveButtonLabel,
                            negativeButtonLabel: negativeButtonLabel,
                            positiveFunctionality: () =>
                                positiveFunctionality(),
                          )),
          );
        },
        transitionDuration: Duration(milliseconds: 200),
        barrierDismissible: isDismissable,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {
          return Container();
        });
  }
}
