import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/scheduler.dart';

class ShowFlushBar {
  String title;
  String message;
  bool isInProgress;
  bool isThisError;
  bool isSuccess;
  double duration;
  BuildContext context;

  ShowFlushBar(
      {@required this.title,
      @required this.message,
      @required this.isInProgress,
      @required this.duration,
      @required this.isSuccess,
      @required this.isThisError,
      @required this.context});

  // FlushBar, Here we're declaring this globally because,
  // if any previous flushbar was shown we will dismiss it first and then show the new flushbar,
  // We will achieve this by using two parameters {flushbar, previous flushbar} we will
  // assign the flushbar to previous flush bar and hide it and show the new flush bar

  /// The current [ShowFlushBar], if one has been created.
  static ShowFlushBar get instance => _instance;
  static ShowFlushBar _instance;

  static Flushbar flushbar, previousFlushBar;
  Future show() async {
    if (flushbar != null) {
      previousFlushBar = flushbar;
      previousFlushBar.dismiss();
    }
    flushbar = new Flushbar(
      title: title,
      message: message,
      showProgressIndicator: isInProgress,
      backgroundColor: isSuccess
          ? Colors.green
          : isThisError ? Colors.red : Colors.black.withOpacity(0.8),
      icon: Icon(
        isSuccess ? Icons.done : isThisError ? Icons.error_outline : Icons.info,
        color: Colors.white,
      ),
      duration: duration != null ? Duration(seconds: duration.toInt()) : null,
    );
    SchedulerBinding.instance.addPostFrameCallback((_) {
      flushbar.dismiss();
      flushbar.show(context);
    });
  }
}
