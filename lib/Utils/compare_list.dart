import 'package:collection/collection.dart';
import 'package:equatable/equatable.dart';
import 'package:sustain_energy/Models/equipment_response.dart'
    as equipmentResponse;
import 'package:sustain_energy/Models/facility_response.dart'
    as facilityResponse;

class Comparator extends Equatable {
  //final String value;
  final List<facilityResponse.Item> items;

  Comparator(this.items);

  @override
  List<Object> get props => [items];
}

class ComparatorForEquipment extends Equatable {
  //final String value;
  final List<equipmentResponse.Item> items;

  ComparatorForEquipment(this.items);

  @override
  List<Object> get props => [items];
}

class ComplexListCompare {
  static List<T> help<T extends dynamic>(
      List<T> redundantList, List<T> newList) {
    assert((newList.isNotEmpty && redundantList.isNotEmpty));
    // Compare the two lists including argumets, ignoring the instance.
    // If list doesn't match, it means either one of the list contains the excess data.
    DeepCollectionEquality _deepCompare = new DeepCollectionEquality();
    bool _result = _deepCompare.equals(redundantList, newList);
    // Now If _result is false we will check and get that particular item which was not present in
    // other list.
    if (!_result) {
      if (DefaultEquality().equals(T, facilityResponse.Item)) {
        // As Dart doesn't interfere automatically like kotlin we need to manually interfere them.
        var ol = Comparator(redundantList as List<facilityResponse.Item>);
        var nl = Comparator(newList as List<facilityResponse.Item>);
        List<facilityResponse.Item> finalList =
            nl.items.toSet().union(ol.items.toSet()).toList();
        finalList.sort((a, b) => b.createdAt.compareTo(a.createdAt));
        return finalList as List<T>;
      } else if (DefaultEquality().equals(T, equipmentResponse.Item)) {
        // As Dart doesn't interfere automatically like kotlin we need to manually interfere them.
        var ol = ComparatorForEquipment(
            redundantList as List<equipmentResponse.Item>);
        var nl =
            ComparatorForEquipment(newList as List<equipmentResponse.Item>);
        List<equipmentResponse.Item> finalList =
            nl.items.toSet().union(ol.items.toSet()).toList();
        finalList.sort((a, b) => b.createdAt.compareTo(a.createdAt));
        return finalList as List<T>;
      } else {
        print(
            '>>>>>>>>>>>>> AS OF NOW WE ONLY SPORTS FACILITY RESPONSE & EQUIPMENT RESPONSE <<<<<<<<<<<<<');
        return [];
      }
    } else {
      // Both Lists are equal.
      return newList;
    }
  }
}
