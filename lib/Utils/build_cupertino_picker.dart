import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sustain_energy/Models/facility_type.dart';

class BuildCupertinoPicker extends StatefulWidget {
  final List<FacilityTypeModel> list;
  final int index;
  final Function callback;
  BuildCupertinoPicker(
      {@required this.index, @required this.list, @required this.callback});
  @override
  _BuildCupertinoPickerState createState() =>
      _BuildCupertinoPickerState(selectedFacilityIndex: index, list: list);
}

class _BuildCupertinoPickerState extends State<BuildCupertinoPicker> {
  final List<FacilityTypeModel> list;
  final int selectedFacilityIndex;
  _BuildCupertinoPickerState(
      {@required this.selectedFacilityIndex, @required this.list});
  FixedExtentScrollController _controller;
  int index = 0;

  @override
  void initState() {
    _controller = new FixedExtentScrollController(initialItem: 0);
    WidgetsBinding.instance.addPostFrameCallback((callback) =>
        _controller.animateToItem(selectedFacilityIndex,
            duration: Duration(milliseconds: 300), curve: Curves.easeInOut));
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
      child: CupertinoPicker.builder(
        backgroundColor: Colors.transparent,
        squeeze: 1,
        scrollController: _controller,
        onSelectedItemChanged: (int) {
          index = int;
          widget.callback(index, list);
        },
        itemExtent: 30,
        diameterRatio: 1.1,
        childCount: list.length,
        itemBuilder: (context, index) {
          return Text(
            list[index].name,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 19,
                color: Colors.black.withOpacity(0.8)),
          );
        },
      ),
      height: 200,
    );
  }
}
