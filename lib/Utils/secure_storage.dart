import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureStorage {
  static FlutterSecureStorage _storage;
  static FlutterSecureStorage getSecureInstance() {
    if (_storage == null) {
      _storage = new FlutterSecureStorage();
      return _storage;
    } else {
      return _storage;
    }
  }
}
