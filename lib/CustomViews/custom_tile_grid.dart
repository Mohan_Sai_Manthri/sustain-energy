import 'package:flutter/material.dart';
import 'package:sustain_energy/CustomViews/custom_cut_grid.dart';
import 'package:sustain_energy/Utils/constants.dart';

class CustomTileGrid extends StatelessWidget {
  final String facilityId;
  final String title;
  final String address;
  final int index;
  final Function clickFunctionality;
  final Function deleteFunctionality;
  final bool isEditable;
  CustomTileGrid(
      {@required this.title,
      @required this.address,
      @required this.index,
      @required this.facilityId,
      @required this.clickFunctionality,
      @required this.deleteFunctionality,
      @required this.isEditable});
  @override
  Widget build(BuildContext context) {
    var stack = !isEditable
        ? Stack(
            alignment: Alignment.center,
            children: <Widget>[
              ClipRRect(
                  borderRadius: BorderRadius.circular(60),
                  clipBehavior: Clip.antiAlias,
                  child: Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Color(0xff53d8d5), Color(0xff52a9e9)]),
                    ),
                    child: Material(
                        type: MaterialType.transparency,
                        child: InkWell(
                            onTap: () {},
                            splashColor: Colors.black.withOpacity(0.5),
                            child: Container())),
                  )),
              Text(
                index.toString(),
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.w600),
              ),
            ],
          )
        : Stack(
            alignment: Alignment.center,
            children: <Widget>[
              ClipRRect(
                  borderRadius: BorderRadius.circular(60),
                  clipBehavior: Clip.antiAlias,
                  child: Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(color: Colors.red),
                    child: Material(
                        type: MaterialType.transparency,
                        child: InkWell(
                            onTap: () {
                              deleteFunctionality(index - 1);
                            },
                            splashColor: Colors.black.withOpacity(0.5),
                            child: Container())),
                  )),
              IconButton(
                icon: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
                onPressed: () {
                  deleteFunctionality(index - 1);
                },
              )
            ],
          );
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () {},
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              InkWell(
                onTap: () {
                  clickFunctionality(facilityId, title);
                },
                child: Container(
                  decoration: BoxDecoration(),
                  margin: EdgeInsets.only(top: Consts.avatarRadius),
                  child: CustomPaint(
                    painter: CustomGridTile(),
                    child: Container(
                      padding: EdgeInsets.only(
                        top: 12 + Consts.padding,
                        bottom: Consts.padding,
                        left: Consts.padding,
                        right: Consts.padding,
                      ),
                      decoration: new BoxDecoration(
                        color: Colors.transparent,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(Consts.padding),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        mainAxisSize:
                            MainAxisSize.min, // To make the card compact
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                title,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                address,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              //...top circlular image part,
              Positioned(
                top: Consts.padding + 10,
                left: Consts.padding + 19,
                right: Consts.padding + 19,
                child: Material(
                  elevation: 5,
                  clipBehavior: Clip.antiAlias,
                  shape: CircleBorder(),
                  child: stack,
                ),
              ),
            ],
          )),
    );
  }
}
