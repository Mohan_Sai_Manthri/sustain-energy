import 'package:flutter/material.dart';

class CustomPath extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Gradient gradient =
        LinearGradient(colors: [Color(0xff53d8d5), Color(0xff52a9e9)]);
    Rect rect = new Rect.fromCircle(
      center: new Offset(210.0, 55.0),
      radius: 180.0,
    );
    Path path = Path();
    Paint paint = Paint()
      ..shader = gradient.createShader(rect)
      ..isAntiAlias = true
      ..color = Colors.teal
      ..style = PaintingStyle.fill;
    path.moveTo(0, size.height / 4);
    path.cubicTo(0, size.height / 4, size.width / 2, size.height, size.width,
        size.height / 4);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
