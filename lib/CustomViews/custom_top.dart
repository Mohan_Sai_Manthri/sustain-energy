import 'package:flutter/material.dart';

import 'custom_path.dart';

class CustomTopContainer extends StatelessWidget {
  final bool isLoading;
  final bool isSuccess;
  CustomTopContainer({this.isLoading, this.isSuccess});
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        CustomPaint(
          painter: CustomPath(),
          child: Container(
            height: 80,
          ),
        ),
        Stack(
          fit: StackFit.loose,
          alignment: Alignment.center,
          children: <Widget>[
            Visibility(
              child: SizedBox(
                width: 64,
                height: 64,
                child: CircularProgressIndicator(
                  strokeWidth: 4,
                  valueColor: AlwaysStoppedAnimation(Theme.of(context).primaryColor),
                ),
              ),
              visible: isLoading != null && isLoading,
            ),
            Material(
              elevation: 8,
              clipBehavior: Clip.antiAlias,
              shape: CircleBorder(),
              child: CircleAvatar(
                maxRadius: 30,
                backgroundColor: Colors.white,
                child: isSuccess != null && isSuccess
                    ? Icon(
                        Icons.done,
                        color: Colors.green,
                        size: 36,
                      )
                    : Image.asset(
                        'assets/images/app_icon.png',
                        scale: 3.5,
                      ),
              ),
            ),
          ],
        )
      ],
    );
  }
}
