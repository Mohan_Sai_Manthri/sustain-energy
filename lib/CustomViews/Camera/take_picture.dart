import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:sustain_energy/Utils/show_flushbar.dart';
import 'package:sustain_energy/Utils/show_gradient_app_bar.dart';

import '../custom_top.dart';

// A screen that allows users to take a picture using a given camera.
class TakePictureScreen extends StatefulWidget {
  final CameraDescription camera;

  const TakePictureScreen({
    Key key,
    @required this.camera,
  }) : super(key: key);

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();
    // To display the current output from the Camera,
    // create a CameraController.
    _controller = CameraController(
      // Get a specific camera from the list of available cameras.
      widget.camera,
      // Define the resolution to use.
      ResolutionPreset.high,
    );

    // Next, initialize the controller. This returns a Future.
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, "");
        return false;
      },
      child: MaterialApp(
        theme: ThemeData(primarySwatch: Colors.teal),
        home: Scaffold(
          appBar: ShowGradientAppBar(
            title: 'Take a picture',
            leadingIcon: OMIcons.arrowBackIos,
            leadIconClickFuncitonality: null,
            actions: null,
          ).show(context),
          // Wait until the controller is initialized before displaying the
          // camera preview. Use a FutureBuilder to display a loading spinner
          // until the controller has finished initializing.
          body: Stack(
            children: <Widget>[
              FutureBuilder<void>(
                future: _initializeControllerFuture,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    // If the Future is complete, display the preview.
                    return CameraPreview(_controller);
                  } else {
                    // Otherwise, display a loading indicator.
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
              CustomTopContainer(),
              SizedBox(
                height: 20,
              ),
            ],
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          floatingActionButton: FloatingActionButton(
            child: Icon(
              Icons.camera_alt,
              color: Colors.white,
            ),
            backgroundColor: Color(0xff53d8d5),
            // Provide an onPressed callback.
            onPressed: () async {
              // Take the Picture in a try / catch block. If anything goes wrong,
              // catch the error.
              try {
                // Ensure that the camera is initialized.
                await _initializeControllerFuture;

                // Construct the path where the image should be saved using the
                // pattern package.
                final path = join(
                  // Store the picture in the temp directory.
                  // Find the temp directory using the `path_provider` plugin.
                  (await getTemporaryDirectory()).path,
                  '${DateTime.now()}.png',
                );

                // Attempt to take a picture and log where it's been saved.
                await _controller.takePicture(path);

                // If the picture was taken, display it on a new screen.
                bool result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          DisplayPictureScreen(imagePath: path),
                    ));
                if (result) {
                  Navigator.pop(context, path == null ? "" : path);
                }
              } catch (e) {
                // If an error occurs, log the error to the console.
                print(e);
                if (e == CameraException ||
                    e.toString() ==
                        'CameraException(cameraPermission, MediaRecorderCamera permission not granted)') {
                  ShowFlushBar(
                          title: 'Permissions required.',
                          message:
                              'please grant permissions required to continue. \n \nyou can do that by reopening this page or in device settings.',
                          context: context,
                          duration: 5.0,
                          isInProgress: false,
                          isSuccess: false,
                          isThisError: false)
                      .show();
                }
              }
            },
          ),
        ),
      ),
    );
  }
}

// A widget that displays the picture taken by the user.
class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ShowGradientAppBar(
        title: 'Preview',
        leadingIcon: Icons.close,
        leadIconClickFuncitonality: null,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.done,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context, true);
            },
          )
        ],
      ).show(context),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Stack(
        children: <Widget>[
          Center(
            child: Image.file(
              File(imagePath),
            ),
          ),
          CustomTopContainer(),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
