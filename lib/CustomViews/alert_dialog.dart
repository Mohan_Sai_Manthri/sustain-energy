import 'package:flutter/material.dart';
import 'package:sustain_energy/Utils/constants.dart';

import 'custom_cut.dart';

class CustomAlertDialog extends Dialog {
  final String title;
  final String content;
  final String positiveButtonLabel;
  final String negativeButtonLabel;
  final Function positiveFunctionality;
  CustomAlertDialog(
      {@required this.title,
      @required this.content,
      @required this.positiveButtonLabel,
      @required this.negativeButtonLabel,
      @required this.positiveFunctionality});
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    // We subtract 80  because of the horizontal padding on Dialog box
    var width = MediaQuery.of(context).size.width - 80;
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () {},
          child: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 10.0,
                      offset: const Offset(0.0, 10.0),
                    ),
                  ],
                ),
                margin: EdgeInsets.only(top: Consts.avatarRadius),
                child: CustomPaint(
                  painter: CustomCut(),
                  child: Container(
                    padding: EdgeInsets.only(
                      top: 8 + Consts.padding,
                      bottom: Consts.padding,
                      left: Consts.padding,
                      right: Consts.padding,
                    ),
                    decoration: new BoxDecoration(
                      color: Colors.transparent,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(Consts.padding),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize:
                          MainAxisSize.min, // To make the card compact
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 8),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              title,
                              style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 50.0),
                        Text(
                              content,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 16.0,
                              ),
                            ),
                        SizedBox(height: 30.0),
                        Align(
                            alignment: Alignment.bottomRight,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                positiveButtonLabel != null
                                    ? FlatButton(
                                        onPressed: positiveFunctionality,
                                        child: Text(
                                          positiveButtonLabel,
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .primaryColor,
                                              fontWeight: FontWeight.w600),
                                        ),
                                      )
                                    : Container(),
                                negativeButtonLabel != null
                                    ? FlatButton(
                                        onPressed: () {
                                          Navigator.of(context)
                                              .pop(); // To close the dialog
                                        },
                                        child: Text(negativeButtonLabel),
                                      )
                                    : Container()
                              ],
                            )),
                      ],
                    ),
                  ),
                ),
              ),

              //...top circlular image part,
              Positioned(
                top: Consts.padding + 10,
                right: width / 10,
                child: Material(
                  elevation: 8,
                  clipBehavior: Clip.antiAlias,
                  shape: CircleBorder(),
                  child: CircleAvatar(
                    maxRadius: 35,
                    backgroundColor: Colors.white,
                    child: Image.asset(
                      'assets/images/app_icon.png',
                      scale: 3.5,
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
