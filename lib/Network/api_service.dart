import 'dart:io';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path/path.dart';
import 'package:sustain_energy/Utils/constants.dart';
import 'package:sustain_energy/Utils/secure_storage.dart';
import 'package:sustain_energy/bloc/bloc.dart';

class ApiService with DioMixin implements Dio {
  static Dio dio;
  static final ApiService service = new ApiService();
  static final secureInstance = SecureStorage.getSecureInstance();
  // "content-type": "application/json" -> Use this if content type is const or
  // if encountered with any error related to content-type.
  static Map<String, String> headers = {};
  Map<String, String> cookies = {};

  static ApiService create() {
    dio = Dio(BaseOptions(
        connectTimeout: 5000, receiveTimeout: 5000, baseUrl: Consts.baseUrl));
    dio.httpClientAdapter = new DefaultHttpClientAdapter();
    // Adding Https certificate to avoid HTTPS Handshake error.
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    restartedSetHeader().then((onValue) {
      dio.options.headers = headers;
    });
    // Return Dio after setup.
    return service;
  }

  // ## Re-generate Header
  // Whenever app restarts we need to re-generate cookie to make session alive and get data from api's.
  // we save our data like token & cookie using secured storage which stores encrypted data.
  static Future restartedSetHeader() async {
    var cookie =
        await SecureStorage.getSecureInstance().read(key: Consts.secureCookie);
    headers['cookie'] = cookie;
  }

  void _setCookie(String rawCookie) {
    if (rawCookie.length > 0) {
      var keyValue = rawCookie.split('=');
      if (keyValue.length == 2) {
        var key = keyValue[0].trim();
        var value = keyValue[1];
        // ignore keys that aren't cookies
        if (key == 'path' || key == 'expires') return;
        this.cookies[key] = value;
      }
    }
  }

  String _generateCookieHeader() {
    String cookie = "";
    for (var key in cookies.keys) {
      if (cookie.length > 0) cookie += ";";
      cookie += key + "=" + cookies[key];
    }
    return cookie;
  }

  Future<void> deleteToken() async {
    //delete token from keystore/keychain
    await secureInstance.delete(key: Consts.secureTokenKey);
    return;
  }

  Future<void> persistToken(String token) async {
    //write token to keystore/keychain
    await secureInstance.write(key: Consts.secureTokenKey, value: token);
    return;
  }

  static Future<bool> hasToken() async {
    var temp = await secureInstance.read(key: Consts.secureTokenKey);
    return temp != null && temp != '';
  }

  Future setupHeader(Response response) async {
    if (response.statusCode == 200) {
      String allSetCookie = response.headers.value('set-cookie');
      if (allSetCookie != null) {
        var setCookies = allSetCookie.split(',');
        for (var setCookie in setCookies) {
          var cookies = setCookie.split(';');
          for (var cookie in cookies) {
            _setCookie(cookie);
          }
        }
        headers['cookie'] = _generateCookieHeader();
      }
      await secureInstance.write(
          key: Consts.secureCookie, value: headers['cookie']);
    }
  }

  // ****************             API REQUESTS            ***************

  // Put & Patch requests are specified the same way - they must contain the @Body
  Future<String> loginUser(Map<String, String> body) async {
    var response = await dio.post(Consts.loginUserPath, data: body);
    // Save token for later use.
    await persistToken(response.data['auth_token']);
    // Set-up header which we will use later on most of the requests.
    await setupHeader(response);
    // Overriding dio instance with new one as we need to add header and use it in later api requests.
    // [NOT SURE] -> can also do like this -> dio.options.headers = headers;
    dio.options.headers = headers;
    return response.data['auth_token'];
  }

  // Logout user
  Future<void> logoutUser() async {
    try {
      await dio.post(Consts.logoutUserPath);
      deleteToken();
    } catch (e) {}
    return;
  }

  // Get All Facilities
  Future<Response> getAllFacilities(int page) async {
    return await dio.get(Consts.facilityPath, queryParameters: {'page': page});
  }

  // Get All Equipments
  Future<Response> getAllEquipments(int page) async {
    return await dio.get(Consts.equipmentPath, queryParameters: {'page': page});
  }

  // Get EquipmentType
  Future<Response> getEquipmentTypes() async {
    return await dio.get(Consts.equipmentTypePath);
  }

  // Get FacilityType
  Future<Response> getFacilityTypes() async {
    return await dio.get(Consts.facilityTypePath);
  }

  // Add Facility
  Future<Response> addFacility(Map<String, dynamic> body) async {
    return await dio.post(Consts.facilityPath, data: body);
  }

  // Add Equipment
  Future<Response> addEquipment(
      Map<String, dynamic> body, BuildContext context) async {
    return await dio.post(Consts.equipmentPath, data: body,
        onSendProgress: (int sent, int total) {
      BlocProvider.of<UploadingBloc>(context).add(StartUploading(
          percentage: ((sent / total) * 100).toStringAsFixed(2),
          typeUploaded: TypeUploaded.CREATED));
    }).catchError((onError) {
      BlocProvider.of<UploadingBloc>(context).add(UploadingFailedEvent(
          error: onError.toString(), typeUploaded: TypeUploaded.UPLOADED));
    });
  }

  // Add Photo to Equipment
  Future<List<Response>> addPhotoToEquipment(BuildContext context,
      File photoFile, File nameplatePhoto, String id) async {
    FormData formData = new FormData.fromMap({
      "files": await MultipartFile.fromFile(photoFile.path,
          filename: basename(photoFile.path)),
    });
    FormData formData_1 = new FormData.fromMap({
      "files": await MultipartFile.fromFile(photoFile.path,
          filename: basename(nameplatePhoto.path)),
    });
    return await Future.wait([
      dio.post('/equipment/$id/photos/', data: formData,
          onSendProgress: (int sent, int total) {
        BlocProvider.of<UploadingBloc>(context).add(StartUploading(
            percentage: ((sent / total) * 100).toStringAsFixed(2),
            typeUploaded: TypeUploaded.UPLOADED));
      }).catchError((onError) {
        BlocProvider.of<UploadingBloc>(context).add(UploadingFailedEvent(
            error: onError.toString().contains('413')
                ? 'Image Size is too large..!'
                : onError.toString(),
            typeUploaded: TypeUploaded.UPLOADED));
      }),
      dio.post('/equipment/$id/nameplate/', data: formData_1,
          onSendProgress: (int sent, int total) {
        BlocProvider.of<UploadingBloc>(context).add(StartUploading(
            percentage: ((sent / total) * 100).toStringAsFixed(2),
            typeUploaded: TypeUploaded.UPLOADED));
      }).catchError((onError) {
        BlocProvider.of<UploadingBloc>(context).add(UploadingFailedEvent(
            error: onError.toString().contains('413')
                ? 'Image Size is too large..!'
                : onError.toString(),
            typeUploaded: TypeUploaded.UPLOADED));
      })
    ]);
  }

  // Delete Equipment
  Future<Response> deleteEquipment(String id) async {
    return await dio.delete('/equipment/$id/');
  }

  // Delete Facility
  Future<Response> deleteFacility(String id) async {
    return await dio.delete('/facility/$id/');
  }
}
