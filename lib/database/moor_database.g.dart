// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'moor_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Facilitie extends DataClass implements Insertable<Facilitie> {
  final String name;
  final String address;
  final String area;
  final int postalCode;
  final String type;
  Facilitie(
      {@required this.name,
      @required this.address,
      @required this.area,
      @required this.postalCode,
      @required this.type});
  factory Facilitie.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final intType = db.typeSystem.forDartType<int>();
    return Facilitie(
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      address:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}address']),
      area: stringType.mapFromDatabaseResponse(data['${effectivePrefix}area']),
      postalCode: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}postal_code']),
      type: stringType.mapFromDatabaseResponse(data['${effectivePrefix}type']),
    );
  }
  factory Facilitie.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Facilitie(
      name: serializer.fromJson<String>(json['name']),
      address: serializer.fromJson<String>(json['address']),
      area: serializer.fromJson<String>(json['area']),
      postalCode: serializer.fromJson<int>(json['postalCode']),
      type: serializer.fromJson<String>(json['type']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'name': serializer.toJson<String>(name),
      'address': serializer.toJson<String>(address),
      'area': serializer.toJson<String>(area),
      'postalCode': serializer.toJson<int>(postalCode),
      'type': serializer.toJson<String>(type),
    };
  }

  @override
  FacilitiesCompanion createCompanion(bool nullToAbsent) {
    return FacilitiesCompanion(
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      address: address == null && nullToAbsent
          ? const Value.absent()
          : Value(address),
      area: area == null && nullToAbsent ? const Value.absent() : Value(area),
      postalCode: postalCode == null && nullToAbsent
          ? const Value.absent()
          : Value(postalCode),
      type: type == null && nullToAbsent ? const Value.absent() : Value(type),
    );
  }

  Facilitie copyWith(
          {String name,
          String address,
          String area,
          int postalCode,
          String type}) =>
      Facilitie(
        name: name ?? this.name,
        address: address ?? this.address,
        area: area ?? this.area,
        postalCode: postalCode ?? this.postalCode,
        type: type ?? this.type,
      );
  @override
  String toString() {
    return (StringBuffer('Facilitie(')
          ..write('name: $name, ')
          ..write('address: $address, ')
          ..write('area: $area, ')
          ..write('postalCode: $postalCode, ')
          ..write('type: $type')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      name.hashCode,
      $mrjc(address.hashCode,
          $mrjc(area.hashCode, $mrjc(postalCode.hashCode, type.hashCode)))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Facilitie &&
          other.name == this.name &&
          other.address == this.address &&
          other.area == this.area &&
          other.postalCode == this.postalCode &&
          other.type == this.type);
}

class FacilitiesCompanion extends UpdateCompanion<Facilitie> {
  final Value<String> name;
  final Value<String> address;
  final Value<String> area;
  final Value<int> postalCode;
  final Value<String> type;
  const FacilitiesCompanion({
    this.name = const Value.absent(),
    this.address = const Value.absent(),
    this.area = const Value.absent(),
    this.postalCode = const Value.absent(),
    this.type = const Value.absent(),
  });
  FacilitiesCompanion.insert({
    @required String name,
    @required String address,
    @required String area,
    @required int postalCode,
    @required String type,
  })  : name = Value(name),
        address = Value(address),
        area = Value(area),
        postalCode = Value(postalCode),
        type = Value(type);
  FacilitiesCompanion copyWith(
      {Value<String> name,
      Value<String> address,
      Value<String> area,
      Value<int> postalCode,
      Value<String> type}) {
    return FacilitiesCompanion(
      name: name ?? this.name,
      address: address ?? this.address,
      area: area ?? this.area,
      postalCode: postalCode ?? this.postalCode,
      type: type ?? this.type,
    );
  }
}

class $FacilitiesTable extends Facilities
    with TableInfo<$FacilitiesTable, Facilitie> {
  final GeneratedDatabase _db;
  final String _alias;
  $FacilitiesTable(this._db, [this._alias]);
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn('name', $tableName, false,
        $customConstraints: 'UNIQUE');
  }

  final VerificationMeta _addressMeta = const VerificationMeta('address');
  GeneratedTextColumn _address;
  @override
  GeneratedTextColumn get address => _address ??= _constructAddress();
  GeneratedTextColumn _constructAddress() {
    return GeneratedTextColumn(
      'address',
      $tableName,
      false,
    );
  }

  final VerificationMeta _areaMeta = const VerificationMeta('area');
  GeneratedTextColumn _area;
  @override
  GeneratedTextColumn get area => _area ??= _constructArea();
  GeneratedTextColumn _constructArea() {
    return GeneratedTextColumn(
      'area',
      $tableName,
      false,
    );
  }

  final VerificationMeta _postalCodeMeta = const VerificationMeta('postalCode');
  GeneratedIntColumn _postalCode;
  @override
  GeneratedIntColumn get postalCode => _postalCode ??= _constructPostalCode();
  GeneratedIntColumn _constructPostalCode() {
    return GeneratedIntColumn(
      'postal_code',
      $tableName,
      false,
    );
  }

  final VerificationMeta _typeMeta = const VerificationMeta('type');
  GeneratedTextColumn _type;
  @override
  GeneratedTextColumn get type => _type ??= _constructType();
  GeneratedTextColumn _constructType() {
    return GeneratedTextColumn(
      'type',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [name, address, area, postalCode, type];
  @override
  $FacilitiesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'facilities';
  @override
  final String actualTableName = 'facilities';
  @override
  VerificationContext validateIntegrity(FacilitiesCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (name.isRequired && isInserting) {
      context.missing(_nameMeta);
    }
    if (d.address.present) {
      context.handle(_addressMeta,
          address.isAcceptableValue(d.address.value, _addressMeta));
    } else if (address.isRequired && isInserting) {
      context.missing(_addressMeta);
    }
    if (d.area.present) {
      context.handle(
          _areaMeta, area.isAcceptableValue(d.area.value, _areaMeta));
    } else if (area.isRequired && isInserting) {
      context.missing(_areaMeta);
    }
    if (d.postalCode.present) {
      context.handle(_postalCodeMeta,
          postalCode.isAcceptableValue(d.postalCode.value, _postalCodeMeta));
    } else if (postalCode.isRequired && isInserting) {
      context.missing(_postalCodeMeta);
    }
    if (d.type.present) {
      context.handle(
          _typeMeta, type.isAcceptableValue(d.type.value, _typeMeta));
    } else if (type.isRequired && isInserting) {
      context.missing(_typeMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {name};
  @override
  Facilitie map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Facilitie.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(FacilitiesCompanion d) {
    final map = <String, Variable>{};
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    if (d.address.present) {
      map['address'] = Variable<String, StringType>(d.address.value);
    }
    if (d.area.present) {
      map['area'] = Variable<String, StringType>(d.area.value);
    }
    if (d.postalCode.present) {
      map['postal_code'] = Variable<int, IntType>(d.postalCode.value);
    }
    if (d.type.present) {
      map['type'] = Variable<String, StringType>(d.type.value);
    }
    return map;
  }

  @override
  $FacilitiesTable createAlias(String alias) {
    return $FacilitiesTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $FacilitiesTable _facilities;
  $FacilitiesTable get facilities => _facilities ??= $FacilitiesTable(this);
  @override
  List<TableInfo> get allTables => [facilities];
}
