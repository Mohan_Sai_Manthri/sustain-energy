import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'moor_database.g.dart';

class Facilities extends Table {
//  address: null,
//  area: null,
//  name: null,
//  postalCode: null,
//  type: null,

  TextColumn get name => text().named('name').customConstraint('UNIQUE')();

  // primary key.
  @override
  Set<Column> get primaryKey => {name};

  TextColumn get address => text()();

  TextColumn get area => text()();

  IntColumn get postalCode => integer()();

  TextColumn get type => text()();
}

@UseMoor(tables: [Facilities])
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(
            path: 'db.sqlite', logStatements: true));

  // Bump this when changing tables and columns.
  // Migrations will be covered in the next part.
  @override
  int get schemaVersion => 1;

  Stream<List<Facilitie>> getAllFacilities() => select(facilities).watch();

  Future addFacility(Facilitie facilitie) => into(facilities).insert(facilitie);

  Future deleteFacility(Facilitie facilitie) =>
      delete(facilities).delete(facilitie);
}
