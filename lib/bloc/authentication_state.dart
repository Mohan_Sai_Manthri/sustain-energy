import 'package:equatable/equatable.dart';

abstract class AuthenticationState extends Equatable {
  const AuthenticationState();
}

class InitialAuthenticationState extends AuthenticationState {
  @override
  List<Object> get props => [];
}

class Uninitialized extends AuthenticationState {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'Uninitialized';
}

class Authenticated extends AuthenticationState {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'Authenticated';
}

class UnAuthenticated extends AuthenticationState {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'UnAuthenticated';
}

class Loading extends AuthenticationState {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'Loading';
}
