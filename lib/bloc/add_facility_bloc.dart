import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import './bloc.dart';

class AddFacilityBloc extends Bloc<AddFacilityEvent, AddFacilityState> {
  final UserRepository userRepository;
  final BuildContext context;
  AddFacilityBloc({@required this.userRepository, @required this.context});
  @override
  AddFacilityState get initialState => UnIntialized();

  @override
  Stream<AddFacilityState> mapEventToState(
    AddFacilityEvent event,
  ) async* {
    if (event is AddFacility) {
      yield DataLoadingAF();
      Future.delayed(Duration(seconds: 3));
      /* [TO-DO]: User repository add facility returns null*/
      /* Needs to fix this*/
      var response = await userRepository.addFacility(context, event.model);
      print(response.name);
      if (response.id != null) {
        yield DataLoadedAF(model: response);
      } else {
        yield DataLoadingFailedAF(error: response.name);
      }
    }
  }
}
