import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import 'package:sustain_energy/Utils/constant.dart';
import './bloc.dart';

class FacilityBloc extends Bloc<FacilityEvent, FacilityState> {
  final UserRepository userRepository;
  final BuildContext context;
  FacilityBloc({this.userRepository, @required this.context})
      : assert(userRepository != null),
        assert(context != null);
  @override
  FacilityState get initialState => InitialFacilityState();

  @override
  Stream<FacilityState> mapEventToState(
    FacilityEvent event,
  ) async* {
    if (event is GetResults) {
      yield DataLoadingFacility();
      var response =
          await userRepository.getResultsFromApi(context, event.page);
      print(response);
      if (response.count == null) {
        yield DataLoadingFailedFacility(
            error:
                response.items[0].address.toString().contains('SocketException')
                    ? 'Internet connection failed'
                    : 'Unauthorized');
      } else {
        yield DataLoadedFacility(model: response);
      }
    } else if (event is InternetConnectionLostInFacility) {
      yield DataLoadingFailedFacility(error: event.error);
    } else if (event is InternetConnectionRestoredFacility) {
      yield DataLoadingFacility();
      if (event.fromState == FromState.GET_EQUIPMENT_RESULTS) {
        var response =
            await userRepository.getEquipmentResultsFromApi(context, 1);
        if (response.count == null) {
          yield DataLoadingFailedFacility(
              error:
                  response.items[0].value.toString().contains('SocketException')
                      ? 'Internet connection failed'
                      : 'Unauthorized');
        } else {
          yield DataLoadedFacility(model: response);
        }
      } else if (event.fromState == FromState.GET_FACILITY_TYPE) {
        var response = await userRepository.getFacilityType(context);
        if (response[0].id == null) {
          yield DataLoadingFailedFacility(error: response[0].name);
        } else {
          yield DataLoadedFacility(model: response);
        }
      } else {
        var response = await userRepository.getResultsFromApi(context, 1);
        if (response.count == null) {
          yield DataLoadingFailedFacility(
              error: response.items[0].address
                      .toString()
                      .contains('SocketException')
                  ? 'Internet connection failed'
                  : 'Unauthorized');
        } else {
          yield DataLoadedFacility(model: response);
        }
      }
    }
  }
}
