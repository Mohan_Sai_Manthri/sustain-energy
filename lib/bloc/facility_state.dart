import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class FacilityState extends Equatable {
  const FacilityState();
}

class InitialFacilityState extends FacilityState {
  @override
  List<Object> get props => [];
}

class DataLoadedFacility extends FacilityState {
  final model;
  DataLoadedFacility({this.model}) : super();
  @override
  List<Object> get props => [model];
  @override
  String toString() {
    return 'Data Loaded \n model: $model';
  }
}

class DataLoadingFacility extends FacilityState {
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'Data Loading';
  }
}

class DataLoadingFailedFacility extends FacilityState {
  final String error;
  DataLoadingFailedFacility({@required this.error}) : super();
  @override
  List<Object> get props => [error];

  @override
  String toString() {
    return 'Data Loading Failed';
  }
}
