import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AddFacilityState extends Equatable {
  const AddFacilityState();
}

class UnIntialized extends AddFacilityState {
  @override
  List<Object> get props => [];
  @override
  String toString() {
    return 'UnInitialized';
  }
}

class DataLoadedAF extends AddFacilityState {
  final model;
  DataLoadedAF({this.model}) : super();
  @override
  List<Object> get props => [model];
  @override
  String toString() {
    return 'Data Loaded \n model: $model';
  }
}

class DataLoadingAF extends AddFacilityState {
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'Data Loading';
  }
}

class DataLoadingFailedAF extends AddFacilityState {
  final String error;
  DataLoadingFailedAF({@required this.error}) : super();
  @override
  List<Object> get props => [error];

  @override
  String toString() {
    return 'Data Loading Failed';
  }
}
