import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import 'package:sustain_energy/Utils/constant.dart';
import './bloc.dart';

class ApiServiceBloc extends Bloc<ApiServiceEvent, ApiServiceState> {
  final UserRepository userRepository;
  final BuildContext context;
  ApiServiceBloc({this.userRepository, @required this.context})
      : assert(userRepository != null),
        assert(context != null);
  @override
  ApiServiceState get initialState => DataLoading();

  @override
  Stream<ApiServiceState> mapEventToState(
    ApiServiceEvent event,
  ) async* {
    if (event is GetEquipmentResults) {
      yield DataLoading();
      var response =
          await userRepository.getEquipmentResultsFromApi(context, event.page);
      if (response == null) {
        yield DataLoadingFailed(error: 'Internet connection failed');
      } else {
        if (response.count == null) {
          yield DataLoadingFailed(
              error:
                  response.items[0].name.toString().contains('SocketException')
                      ? 'Internet connection failed'
                      : 'Unauthorized');
        } else {
          yield DataLoaded(model: response);
        }
      }
    } else if (event is GetFacilityTypes) {
      var response = await userRepository.getFacilityType(context);
      if (response[0].id == null) {
        yield DataLoadingFailed(error: response[0].name);
      } else {
        yield DataLoaded(model: response);
      }
    } else if (event is AddPhotos) {
      yield DataLoading();
      var response = await userRepository.addPhotosToEquipment(
          event.photoPath, event.nameplatePath, context, event.id);
      if (response != null) {
        if (response[0].statusCode == 201 && response[1].statusCode == 201) {
          yield DataLoaded(model: response);
        } else {
          if (response[0].statusCode != 201) {
            yield DataLoadingFailed(error: response[0].data);
          } else {
            yield DataLoadingFailed(error: response[1].data);
          }
        }
      }
    } else if (event is InternetConnectionLost) {
      yield DataLoadingFailed(error: event.error);
    } else if (event is InternetConnectionRestored) {
      yield DataLoading();
      if (event.fromState == FromState.GET_EQUIPMENT_RESULTS) {
        var response =
            await userRepository.getEquipmentResultsFromApi(context, 1);
        if (response == null) {
          yield DataLoadingFailed(error: 'Internet connection failed');
        } else {
          if (response.count == null) {
            yield DataLoadingFailed(
                error: response.items[0].name
                        .toString()
                        .contains('SocketException')
                    ? 'Internet connection failed'
                    : 'Unauthorized');
          } else {
            yield DataLoaded(model: response);
          }
        }
      } else if (event.fromState == FromState.GET_FACILITY_TYPE) {
        var response = await userRepository.getFacilityType(context);
        if (response[0].id == null) {
          yield DataLoadingFailed(error: response[0].name);
        } else {
          yield DataLoaded(model: response);
        }
      } else {
        var response = await userRepository.getResultsFromApi(context, 1);
        if (response.count == null) {
          yield DataLoadingFailed(
              error: response.items[0].address
                      .toString()
                      .contains('SocketException')
                  ? 'Internet connection failed'
                  : 'Unauthorized');
        } else {
          yield DataLoaded(model: response);
        }
      }
    } else if (event is PostEquipment) {
      yield DataLoading();
      var response = await userRepository.addEquipment(event.body, context);
      if (response.id == null) {
        yield DataLoadingFailed(
            error: response.name.toString().contains('SocketException')
                ? 'Internet connection failed'
                : 'Unauthorized');
      } else {
        yield DataLoaded(model: response);
      }
    }
  }
}
