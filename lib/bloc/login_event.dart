import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class AuthenticateUser extends LoginEvent {
  final String username;
  final String password;
  AuthenticateUser({@required this.username, @required this.password})
      : super();
  @override
  List<Object> get props => [username, password];

  @override
  String toString() {
    return 'Authenticate User { username: $username , password: $password }';
  }
}

