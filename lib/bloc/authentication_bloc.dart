import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import './bloc.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository userRepository;
  final BuildContext context;
  AuthenticationBloc({@required this.userRepository, @required this.context})
      : assert(UserRepository != null);
  @override
  AuthenticationState get initialState => Uninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      yield Uninitialized();
    } else if (event is LoggedIn) {
      yield Loading();
      yield Authenticated();
    } else if (event is Authenticate) {
      yield Loading();
      yield Authenticated();
    } else if (event is UnAuthenticate) {
      yield Loading();
      yield UnAuthenticated();
    } else if (event is LoggedOut) {
      yield Loading();
      await userRepository.logoutUser(context);
      yield UnAuthenticated();
    }
  }
}
