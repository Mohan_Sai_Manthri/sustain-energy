import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class LoginState extends Equatable {
  const LoginState();
}

class LoginInitial extends LoginState {
  @override
  List<Object> get props => null;
  @override
  String toString() {
    return 'LoginInitial';
  }
}

class LoginLoading extends LoginState {
  @override
  List<Object> get props => null;
  @override
  String toString() {
    return 'Loading';
  }
}

class LoginFailure extends LoginState {
  final String error;
  LoginFailure({@required this.error}) : super();

  @override
  List<Object> get props => [error];
  @override
  String toString() {
    return 'LoginFailure { error: $error }';
  }
}
