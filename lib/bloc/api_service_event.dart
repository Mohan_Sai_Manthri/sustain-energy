import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sustain_energy/Utils/constant.dart';

abstract class ApiServiceEvent extends Equatable {
  const ApiServiceEvent();
}

class GetEquipmentResults extends ApiServiceEvent {
  final int page;
  GetEquipmentResults({@required this.page});
  @override
  List<Object> get props => [page];

  @override
  String toString() {
    return 'Get Equipment Results $page';
  }
}

class PostEquipment extends ApiServiceEvent {
  final Map<String, dynamic> body;
  PostEquipment({@required this.body});
  @override
  List<Object> get props => [body];

  @override
  String toString() {
    return 'Post Equipment';
  }
}

// class DeleteEquipment extends ApiServiceEvent {
//   final String id;
//   DeleteEquipment({@required this.id});
//   @override
//   List<Object> get props => [id];

//   @override
//   String toString() {
//     return 'Delete Equipment';
//   }
// }

class AddPhotos extends ApiServiceEvent {
  final String id;
  final String photoPath;
  final String nameplatePath;
  AddPhotos(
      {@required this.id,
      @required this.nameplatePath,
      @required this.photoPath});
  @override
  List<Object> get props => [id, photoPath, nameplatePath];
}

class GetFacilityTypes extends ApiServiceEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'Get Facility Types';
  }
}

class InternetConnectionLost extends ApiServiceEvent {
  final String error;
  InternetConnectionLost({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() {
    return "Internet Connection Lost";
  }
}

class InternetConnectionRestored extends ApiServiceEvent {
  final FromState fromState;
  InternetConnectionRestored({@required this.fromState});
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return "Internet Connection Restored";
  }
}
