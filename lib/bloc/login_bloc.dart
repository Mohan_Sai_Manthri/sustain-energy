import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sustain_energy/Repository/user_repository.dart';

import './bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository userRepository;
  final AuthenticationBloc authenticationBloc;
  final BuildContext context;

  LoginBloc(
      {@required this.userRepository,
      @required this.authenticationBloc,
      @required this.context})
      : assert(userRepository != null),
        assert(authenticationBloc != null),
        assert(context != null);

  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is AuthenticateUser) {
      yield LoginLoading();
      try {
        final token = await userRepository.authenticate(
            username: event.username,
            password: event.password,
            context: context);
        if (!token.contains('error: ')) {
          authenticationBloc.add(LoggedIn(token: token));
        } else {
          yield LoginFailure(error: token.replaceAll('error: ', ''));
        }
        yield LoginInitial();
      } catch (err) {
        yield LoginFailure(error: err.toString());
      }
    }
  }
}
