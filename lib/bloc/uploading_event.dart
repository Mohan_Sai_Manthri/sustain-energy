import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sustain_energy/bloc/uploading_state.dart';

abstract class UploadingEvent extends Equatable {
  const UploadingEvent();
}

class StartUploading extends UploadingEvent {
  final String percentage;
  final TypeUploaded typeUploaded;
  StartUploading({@required this.percentage, @required this.typeUploaded})
      : super();
  @override
  List<Object> get props => [percentage, typeUploaded];

  @override
  String toString() {
    return 'Start Uploading';
  }
}

class UploadedEvent extends UploadingEvent {
  final TypeUploaded type;
  UploadedEvent({@required this.type}) : super();
  @override
  List<Object> get props => null;
  @override
  String toString() {
    return 'Uploaded';
  }
}

class UploadingFailedEvent extends UploadingEvent {
  final String error;
  final TypeUploaded typeUploaded;
  UploadingFailedEvent({@required this.error, @required this.typeUploaded})
      : super();

  @override
  List<Object> get props => [error, typeUploaded];
  @override
  String toString() {
    return 'Uploading Failed';
  }
}
