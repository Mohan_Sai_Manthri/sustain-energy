import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sustain_energy/Utils/constant.dart';

abstract class GetequipmenttypeEvent extends Equatable {
  const GetequipmenttypeEvent();
}

class GetEquipmentTypes extends GetequipmenttypeEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'Get Equipment Types';
  }
}

class InternetConnectionLostGE extends GetequipmenttypeEvent {
  final String error;
  InternetConnectionLostGE({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() {
    return "Internet Connection Lost";
  }
}

class InternetConnectionRestoredGE extends GetequipmenttypeEvent {
  final FromState fromState;
  InternetConnectionRestoredGE({@required this.fromState});
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return "Internet Connection Restored";
  }
}
