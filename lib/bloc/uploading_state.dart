import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class UploadingState extends Equatable {
  const UploadingState();
}

enum TypeUploaded { CREATED, UPLOADED }

class Uploading extends UploadingState {
  final String percentage;
  final TypeUploaded type;
  Uploading({@required this.percentage, @required this.type}) : super();
  @override
  List<Object> get props => [percentage, type];
}

class Uploaded extends UploadingState {
  final TypeUploaded type;
  Uploaded({@required this.type}) : super();
  @override
  List<Object> get props => [type];
}

class UploadingFailed extends UploadingState {
  final String error;
  final TypeUploaded typeUploaded;
  UploadingFailed({@required this.error, @required this.typeUploaded})
      : super();
  @override
  List<Object> get props => [error, typeUploaded];
}
