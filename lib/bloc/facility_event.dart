import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sustain_energy/Utils/constant.dart';

abstract class FacilityEvent extends Equatable {
  const FacilityEvent();
}

class GetResults extends FacilityEvent {
  final int page;
  GetResults({@required this.page});
  @override
  List<Object> get props => [page];

  @override
  String toString() {
    return 'Get Results $page';
  }
}

class InternetConnectionLostInFacility extends FacilityEvent {
  final String error;
  InternetConnectionLostInFacility({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() {
    return "Internet Connection Lost";
  }
}

class InternetConnectionRestoredFacility extends FacilityEvent {
  final FromState fromState;
  InternetConnectionRestoredFacility({@required this.fromState});
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return "Internet Connection Restored";
  }
}
