import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AddFacilityEvent extends Equatable {
  const AddFacilityEvent();
}

class Initialize extends AddFacilityEvent{
   @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'Initialize Add Facility';
  }
}
class AddFacility extends AddFacilityEvent {
  final Map<String, dynamic> model;
  AddFacility({@required this.model});
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'Add Facility';
  }
}
