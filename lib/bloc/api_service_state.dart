import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ApiServiceState extends Equatable {
  const ApiServiceState();
}

class InitialApiServiceState extends ApiServiceState {
  @override
  List<Object> get props => [];
}

class DataLoaded extends ApiServiceState {
  final model;
  DataLoaded({this.model}) : super();
  @override
  List<Object> get props => [model];
  @override
  String toString() {
    return 'Data Loaded \n model: $model';
  }
}

class DataLoading extends ApiServiceState {
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'Data Loading';
  }
}

class DataLoadingFailed extends ApiServiceState {
  final String error;
  DataLoadingFailed({@required this.error}) : super();
  @override
  List<Object> get props => [error];

  @override
  String toString() {
    return 'Data Loading Failed';
  }
}
