import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import 'package:sustain_energy/Utils/constant.dart';
import './bloc.dart';

class GetequipmenttypeBloc
    extends Bloc<GetequipmenttypeEvent, GetequipmenttypeState> {
  final UserRepository userRepository;
  final BuildContext context;
  GetequipmenttypeBloc({@required this.context, @required this.userRepository});
  @override
  GetequipmenttypeState get initialState => InitialGetequipmenttypeState();

  @override
  Stream<GetequipmenttypeState> mapEventToState(
    GetequipmenttypeEvent event,
  ) async* {
    if (event is GetEquipmentTypes) {
      yield DataLoadingGE();
      var response = await userRepository.getEquipmentType(context);
      if (response[0].id == null) {
        yield DataLoadingFailedGE(error: response[0].name);
      } else {
        yield DataLoadedGE(model: response);
      }
    } else if (event is InternetConnectionLostGE) {
      yield DataLoadingFailedGE(error: event.error);
    } else if (event is InternetConnectionRestoredGE) {
      yield DataLoadingGE();
      if (event.fromState == FromState.GET_EQUIPMENT_TYPE) {
        var response = await userRepository.getEquipmentType(context);
        if (response[0].id == null) {
          yield DataLoadingFailedGE(error: response[0].name);
        } else {
          yield DataLoadedGE(model: response);
        }
      }
    }
  }
}
