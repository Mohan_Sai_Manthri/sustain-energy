import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:sustain_energy/bloc/uploading_state.dart';
import './bloc.dart';

class UploadingBloc extends Bloc<UploadingEvent, UploadingState> {
  @override
  UploadingState get initialState =>
      Uploading(percentage: '0', type: TypeUploaded.CREATED);

  @override
  Stream<UploadingState> mapEventToState(
    UploadingEvent event,
  ) async* {
    if (event is StartUploading) {
      yield Uploading(percentage: event.percentage, type: event.typeUploaded);
    } else if (event is UploadedEvent) {
      yield Uploaded(type: event.type);
    } else if (event is UploadingFailedEvent) {
      yield UploadingFailed(
          error: event.error, typeUploaded: event.typeUploaded);
    }
  }
}
