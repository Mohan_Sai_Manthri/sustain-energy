import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sustain_energy/Models/equipment_type_model.dart';

abstract class GetequipmenttypeState extends Equatable {
  const GetequipmenttypeState();
}

class InitialGetequipmenttypeState extends GetequipmenttypeState {
  @override
  List<Object> get props => [];
}

class DataLoadedGE extends GetequipmenttypeState {
  final List<EquipmentTypeModel> model;
  DataLoadedGE({this.model}) : super();
  @override
  List<Object> get props => [model];
  @override
  String toString() {
    return 'Data Loaded \n model: $model';
  }
}

class DataLoadingGE extends GetequipmenttypeState {
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'Data Loading';
  }
}

class DataLoadingFailedGE extends GetequipmenttypeState {
  final String error;
  DataLoadingFailedGE({@required this.error}) : super();
  @override
  List<Object> get props => [error];

  @override
  String toString() {
    return 'Data Loading Failed';
  }
}
