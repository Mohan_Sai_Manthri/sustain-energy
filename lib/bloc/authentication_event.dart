import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();
}

class AppStarted extends AuthenticationEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'AppStarted';
}



class Authenticate extends AuthenticationEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'Authenticate';
}

class UnAuthenticate extends AuthenticationEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'UnAuthenticate';
}

class LoggedIn extends AuthenticationEvent {
  final String token;
  LoggedIn({@required this.token}) : super();
  @override
  List<Object> get props => [token];

  @override
  String toString() => 'LoggedIn {token : $token}';
}

class LoggedOut extends AuthenticationEvent {
  @override
  List<Object> get props => null;
  @override
  String toString() {
    return 'LoggedOut';
  }
}
