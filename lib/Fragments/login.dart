import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sustain_energy/Fragments/loginform.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import 'package:sustain_energy/bloc/bloc.dart';

class LoginPage extends StatelessWidget {
  final UserRepository userRepository;
  final BuildContext mContext;
  LoginPage({Key key, @required this.userRepository, @required this.mContext})
      : assert(userRepository != null),
        assert(mContext != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        builder: (context) {
          return LoginBloc(
              authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
              userRepository: userRepository,
              context: mContext);
        },
        child: LoginForm(),
      ),
    );
  }
}
