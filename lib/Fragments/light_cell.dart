import 'package:flutter/material.dart';
import 'package:sustain_energy/Models/equipment_response.dart';
import 'package:sustain_energy/Network/api_service.dart';
import 'package:sustain_energy/Utils/network_image_ssl.dart';

class LightCell extends StatelessWidget {
  const LightCell(
      {Key key,
      @required this.item,
      @required this.index,
      @required this.function,
      @required this.getIndex})
      : super(key: key);

  final Item item;
  final int index;
  final Function function;
  final Function getIndex;

  @override
  Widget build(BuildContext context) {
    Properties path = item.properties;
    return new AnimatedContainer(
      alignment: Alignment.topLeft,
      curve: Curves.easeInOut,
      duration: Duration(milliseconds: 350),
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
      child: Material(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
        child: InkWell(
          onTap: () {
            function(index);
          },
          onLongPress: () {},
          child: AnimatedContainer(
            curve: Curves.easeInOut,
            height: getIndex() != index || getIndex() == null ? 170 : 360,
            duration: Duration(milliseconds: 350),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
            ),
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: getIndex() != index || getIndex() == null
                ? Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      AnimatedContainer(
                        curve: Curves.easeInBack,
                        duration: Duration(milliseconds: 350),
                        width: width(context),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: buildImage()),
                      ),
                      buildSizedBox(),
                      Flexible(
                          child: buildColumn([
                        buildAlign(),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            buildPadding([
                              buildFlexible(
                                  'Area Servicing : ${path.areaServing}',
                                  context),
                              buildSizedBox(),
                              buildFlexible(
                                  'Lights Count : ${path.lightsCount}', context)
                            ]),
                            buildPadding([
                              buildFlexible(
                                  'Light Type : ${path.type}', context),
                              buildSizedBox(),
                            ]),
                            buildPadding([
                              buildSizedBox(),
                            ]),
                          ],
                        )
                      ])),
                    ],
                  )
                : Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      AnimatedContainer(
                        curve: Curves.easeInBack,
                        duration: Duration(milliseconds: 350),
                        width: width(context),
                        height: getIndex() != index || getIndex() == null
                            ? 140
                            : 220,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: buildImage(),
                        ),
                      ),
                      buildSizedBox(),
                      Flexible(
                        child: buildColumn([
                          buildAlign(),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              buildPadding([
                                buildFlexible(
                                    'Area Servicing : ${path.areaServing}',
                                    context),
                                buildSizedBox(),
                                buildFlexible(
                                    'Lights Count : ${path.lightsCount}',
                                    context)
                              ]),
                              buildPadding([
                                buildFlexible(
                                    'Light Type : ${path.type}', context),
                                buildSizedBox(),
                              ]),
                              buildPadding([
                                buildSizedBox(),
                              ]),
                            ],
                          )
                        ]),
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  Column buildColumn(List<Widget> widgets) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween, children: widgets);
  }

  Align buildAlign() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Text(
        item.name,
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
        softWrap: true,
        textAlign: TextAlign.start,
        style: TextStyle(
            color: Colors.black.withOpacity(0.7),
            fontSize: 18,
            fontWeight: FontWeight.w700),
      ),
    );
  }

  double width(BuildContext context) {
    return getIndex() != index || getIndex() == null
        ? 130
        : MediaQuery.of(context).size.width - 10;
  }

  FadeInImage buildImage() {
    return FadeInImage(
      placeholder: AssetImage('assets/images/placeholder.png'),
      height: getIndex() != index || getIndex() == null ? 170 : 360,
      image: item.photos != null &&
              item.photos.isNotEmpty &&
              ApiService.headers != null
          ? NetworkImageSSL(
              'https://167.71.58.236${item.photos[0].url != '' ? item.photos[0].url : item.photos.length >= 1 ? item.photos[1].url : ''}',
              headers: ApiService.headers,
            )
          : NetworkImage(
              'https://developers.google.com/maps/documentation/maps-static/images/error-image-generic.png',
            ),
      // loadingBuilder: (BuildContext context, Widget child,
      //     ImageChunkEvent loadingProgress) {
      //   if (loadingProgress == null) return child;
      //   return Padding(
      //     padding: const EdgeInsets.all(10.0),
      //     child: Center(
      //         child: CircularProgressIndicator(
      //       value: loadingProgress.expectedTotalBytes != null
      //           ? loadingProgress.cumulativeBytesLoaded /
      //               loadingProgress.expectedTotalBytes
      //           : null,
      //     )),
      //   );
      // },
      fit: BoxFit.cover,
    );
  }

  Padding buildPadding(List<Widget> widgets) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: widgets,
      ),
    );
  }

  Flexible buildFlexible(String label, BuildContext context) {
    return Flexible(
      child: Container(
        width: (MediaQuery.of(context).size.width - 10) / 2,
        child: Text(
          label,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          softWrap: true,
          textAlign: TextAlign.start,
          style: TextStyle(
              color: Colors.black.withOpacity(0.7),
              fontSize: 14,
              fontWeight: FontWeight.w500),
        ),
      ),
    );
  }

  SizedBox buildSizedBox() {
    return SizedBox(
      width: 10,
      height: 10,
    );
  }
}
