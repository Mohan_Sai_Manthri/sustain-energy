import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:sustain_energy/CustomViews/custom_tile_grid.dart';
import 'package:sustain_energy/CustomViews/custom_top.dart';
import 'package:sustain_energy/Fragments/add_facility.dart';
import 'package:sustain_energy/Fragments/equipments.dart';
import 'package:sustain_energy/Models/facility_response.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import 'package:sustain_energy/Utils/compare_list.dart';
import 'package:sustain_energy/Utils/constant.dart';
import 'package:sustain_energy/Utils/show_alert_dialog.dart';
import 'package:sustain_energy/Utils/show_flushbar.dart';
import 'package:sustain_energy/Utils/show_gradient_app_bar.dart';
import 'package:sustain_energy/bloc/authentication_bloc.dart';
import 'package:sustain_energy/bloc/authentication_event.dart';
import 'package:sustain_energy/bloc/bloc.dart';

class SelectMainFacility extends StatefulWidget {
  final UserRepository userRepository;
  final BuildContext mContext;
  SelectMainFacility({this.userRepository, this.mContext});
  @override
  _SelectMainFacilityState createState() => _SelectMainFacilityState();
}

class _SelectMainFacilityState extends State<SelectMainFacility>
    with WidgetsBindingObserver {
  bool _isEditable = false;
  FacilityResponse response;
  int maxPage;
  int page = 1;
  int maxCount;
  List<Item> list = [];
  bool _isLoading = false;
  final ValueNotifier<bool> _notifier = ValueNotifier<bool>(true);
  final ValueNotifier<int> _isLoaded = ValueNotifier<int>(0);
  ScrollController _scrollController;
  String _error = '';
  FacilityBloc facilityBloc;

  @override
  void initState() {
    facilityBloc = BlocProvider.of<FacilityBloc>(context);
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      facilityBloc.add(GetResults(page: 1));
    });
    _scrollController = new ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    // Clear the data before leaving or else there is a chance of duplicacy.
    list.clear();
    response.items.clear();
    _scrollController.dispose();
    super.dispose();
  }

  final GlobalKey<AnimatedListState> listKey = GlobalKey<AnimatedListState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent ==
          _scrollController.position.pixels) {
        if (!_isLoading) {
          _isLoading = !_isLoading;
          _isLoaded.value = 0;
          //Do API call here...
          if (page + 1 <= maxPage) {
            facilityBloc.add(GetResults(page: page + 1));
          } else {
            _isLoaded.value = 1;
          }
          page = page + 1;
        }
      }
    });
    return Scaffold(
        key: _scaffoldKey,
        appBar: ShowGradientAppBar(
          title: 'Your Facility',
          leadingIcon: OMIcons.powerSettingsNew,
          leadIconClickFuncitonality: () {
            ShowAlertDialog(
                isDismissable: true,
                title: 'Alert',
                type: DialogTypes.WITH_POSITIVE_NEGATIVE_BUTTON,
                message: 'Are you sure you want to logout?',
                negativeButtonLabel: 'cancel',
                negativeFunctionality: () {
                  Navigator.pop(context);
                },
                positiveButtonLabel: 'continue',
                positiveFunctionality: () {
                  Navigator.pop(context);
                  list.clear();
                  BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
                }).show(context);
          },
          actions: <Widget>[
            IconButton(
              onPressed: () {
                _isEditable = !_isEditable;
                setState(() {});
              },
              icon: Icon(_isEditable ? Icons.close : Icons.edit),
            )
          ],
        ).show(context),
        backgroundColor: Colors.grey[200],
        body: BlocListener<FacilityBloc, FacilityState>(
          listener: (context, state) {
            if (state is DataLoadingFacility) {
              _notifier.value = true;
              _isLoaded.value = 0;
            } else if (state is DataLoadedFacility) {
              dataLoadedInitList(state);
            } else if (state is DataLoadingFailedFacility) {
              _notifier.value = false;
              _isLoaded.value = 2;
              _error = state.error.toString();
              buildErrorBar(context, state);
            }
          },
          child: BlocBuilder<FacilityBloc, FacilityState>(
            builder: (context, state) {
              if (state is DataLoadingFacility) {
                _notifier.value = true;
                _isLoaded.value = 0;
              } else if (state is DataLoadedFacility) {
                dataLoadedInitList(state);
              } else if (state is DataLoadingFailedFacility) {
                _notifier.value = false;
                _error = state.error.contains('Unauthorized')
                    ? 'You\'ve been logged out, please login again.'
                    : state.error.contains('SocketException')
                        ? 'We\'re facing trouble to connect to the Internet'
                        : state.error.toString();
                buildErrorBar(context, state);
                _isLoaded.value = 2;
              }
              return ListView(
                controller: _scrollController,
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                children: <Widget>[
                  ValueListenableBuilder(
                    valueListenable: _notifier,
                    builder: (context, val, child) {
                      return CustomTopContainer(
                        isLoading: val,
                      );
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Center(
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 80, vertical: 5),
                        child: MaterialButton(
                          minWidth: 120,
                          height: 45,
                          textColor: Colors.blue,
                          elevation: 10,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          onPressed: () async {
                            var result = await Navigator.push(context,
                                CupertinoPageRoute(builder: (context) {
                              return AddNewFacility(
                                mContext: context,
                                userRepository: widget.userRepository,
                              );
                            }));
                            if (result != null) {
                              if (result) {
                                list.clear();
                                facilityBloc.add(GetResults(page: 1)); 
                              }
                            }
                          },
                          color: Colors.white,
                          child: Center(
                              child: Text(
                            'Add New Facility',
                            style: TextStyle(color: Colors.grey),
                          )),
                        ),
                      ),
                    ),
                  ),
                  ValueListenableBuilder(
                    valueListenable: _isLoaded,
                    builder: (context, val, child) {
                      return val == 1 || val == 0 && (response != null)
                          ? response.count == 0
                              ? getEmptyListWidget()
                              : getList(context, _isEditable, response)
                          : buildErrorView(_error);
                    },
                  ),
                  ValueListenableBuilder(
                    valueListenable: _isLoaded,
                    builder: (context, val, child) {
                      return val == 0
                          ? Center(
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: CircularProgressIndicator(),
                              ),
                            )
                          : Container();
                    },
                  )
                ],
              );
            },
          ),
        ));
  }

  void dataLoadedInitList(DataLoadedFacility state) {
    if (state.model is FacilityResponse) {
      _notifier.value = false;
      _isLoaded.value = 1;
      response = state.model is FacilityResponse ? state.model : null;
      maxPage = response.pages;
      maxCount = response.count;
      _isLoading = false;
      if (list.isEmpty) {
        list.addAll(response.items);
      } else {
        List<Item> filteredList =
            ComplexListCompare.help<Item>(list, response.items);
        if (filteredList.isNotEmpty) {
          var newList = new List<Item>();
          newList.addAll(filteredList);
          list.clear();
          list.addAll(newList);
        }
        response.items.clear();
        response.items.addAll(list);
      }
    }
    if (ShowFlushBar.flushbar != null) {
      ShowFlushBar.flushbar.dismiss();
    }
  }

  void buildErrorBar(BuildContext context, DataLoadingFailedFacility state) {
    ShowFlushBar(
            context: context,
            duration: null,
            isInProgress: false,
            isSuccess: false,
            isThisError: true,
            message: state.error.contains('SocketException')
                ? 'We\'re facing trouble to connect to the Internet'
                : '${state.error}',
            title: 'Data Loading Failure')
        .show();
  }

  Widget getList(
      BuildContext mContext, bool _isEditable, FacilityResponse model) {
    /*24 is for notification bar on Android*/
    // var size = MediaQuery.of(context).size;
    // final double itemHeight = (size.height - kToolbarHeight - 24) / 2.3;
    // final double itemWidth = size.width / 2;

    return StaggeredGridView.countBuilder(
      key: listKey,
      crossAxisCount: 4,
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
      staggeredTileBuilder: (index) => new StaggeredTile.fit(2),
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemCount: model.items.length,
      itemBuilder: (context, index) {
        return getTile(model, index, context);
      },
    );

    // return GridView.builder(
    //   key: listKey,
    //   gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
    //     crossAxisCount: 2,
    //     childAspectRatio: (itemWidth / itemHeight),
    //   ),
    //   shrinkWrap: true,
    //   physics: ClampingScrollPhysics(),
    //   itemCount: model.items.length,
    //   itemBuilder: (context, index) {
    //     return getTile(model, index, context);
    //   },
    // );
  }

  void deleteFunctionality(int index) async {
    ShowAlertDialog(
            title: 'Confirm',
            message: 'Are you sure you wish to delete this item?',
            positiveButtonLabel: 'DELETE',
            negativeButtonLabel: 'CANCEL',
            positiveFunctionality: () {
              positiveButtonFunctionality(response, index);
            },
            negativeFunctionality: null,
            isDismissable: true,
            type: DialogTypes.WITH_POSITIVE_NEGATIVE_BUTTON)
        .show(context);
  }

  void clickFunctionality(String facilityId, String facilityLabel) {
    Navigator.push(context, CupertinoPageRoute(builder: (context) {
      return EquipmentsPage(
        facilityLabel: facilityLabel,
        userRepository: widget.userRepository,
        mContext: widget.mContext,
        facilityId: facilityId,
      );
    }));
  }

  Future positiveButtonFunctionality(FacilityResponse data, int index) async {
    // var response = await widget.userRepository
    //     .deleteEquipment(data.items[index].id, context);
    // if (response.statusCode == 204 || response.statusCode == 200) {
    //   Scaffold.of(context).showSnackBar(
    //       SnackBar(content: Text("${data.items[index].name} dismissed")));
    //   // Removes that item the list on swipwe
    //   setState(() {
    //     list.removeAt(index);
    //     _isLoaded.value = 0;
    //     _isLoaded.value = 1;
    //   });
    //   Navigator.of(context).pop(true);
    //   // Use the below code if you find any issues after deleting item,
    //   // The below code will ensure that there should be no errors.
    //   //apiServiceBloc.add(GetEquipmentResults());
    // } else if (response.statusCode == 404) {
    //   Scaffold.of(context).showSnackBar(SnackBar(
    //       content:
    //           Text("No Equipment Found on Database with this equipment id")));
    // } else {
    //   Scaffold.of(context).showSnackBar(SnackBar(
    //       content: Text(
    //           "We're facing trouble to delete this item, please try again later.")));
    // }
    Navigator.pop(context);
    ShowFlushBar(
            context: context,
            duration: 3,
            isInProgress: false,
            isSuccess: false,
            isThisError: true,
            title: 'This feature is not yet ready.',
            message: 'We\'re working on this, will be available soon..!')
        .show();
  }

  getTile(FacilityResponse model, int index, BuildContext context) {
    // var _random = new Random();
    // var min = 240;
    // var max = 340;
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: CustomTileGrid(
          title: model.items[index].name,
          address: model.items[index].address,
          index: index + 1,
          deleteFunctionality: deleteFunctionality,
          clickFunctionality: clickFunctionality,
          isEditable: _isEditable,
          facilityId: model.items[index].id,
        ),
      ),
      height: index % 2 == 0 ? 280 : 330,
    );
  }

  getEmptyListWidget() {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 50,
        ),
        Image.asset(
          'assets/images/empty.png',
          height: 300,
          width: 300,
          fit: BoxFit.cover,
        ),
        SizedBox(
          height: 50,
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            'Please list your establishment first to add all equipments.',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.grey.withOpacity(0.5),
                fontWeight: FontWeight.w600,
                fontSize: 18),
          ),
        )
      ],
    );
  }

  Widget buildErrorView(String error) {
    final err = error;
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 150,
          ),
          Text(
            err.contains('Unauthorized')
                ? 'You\'ve been logged out, please login again.'
                : err.contains('SocketException')
                    ? 'We\'re facing trouble to connect to the Internet'
                    : err.toString(),
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: OutlineButton(
              onPressed: () {
                err.contains('Unauthorized') || err.contains('logged out')
                    ? BlocProvider.of<AuthenticationBloc>(context)
                        .add(LoggedOut())
                    : BlocProvider.of<FacilityBloc>(context).add(
                        InternetConnectionRestoredFacility(
                            fromState: FromState.GET_RESULTS));
              },
              child: Text(
                  err.contains('Unauthorized') || err.contains('logged out')
                      ? 'Login Again'
                      : 'Retry'),
            ),
          )
        ],
      ),
    );
  }
}
