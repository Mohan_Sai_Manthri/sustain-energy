import 'dart:io';
import 'package:camera/camera.dart';
import 'package:dio/dio.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:sustain_energy/CustomViews/Camera/take_picture.dart';
import 'package:sustain_energy/CustomViews/custom_cut.dart';
import 'package:sustain_energy/CustomViews/custom_top.dart';
import 'package:sustain_energy/Models/add_equipment_respone.dart';
import 'package:sustain_energy/Models/facility_type.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import 'package:sustain_energy/Utils/build_cupertino_picker.dart';
import 'package:sustain_energy/Utils/constant.dart';
import 'package:sustain_energy/Utils/constants.dart';
import 'package:sustain_energy/Utils/show_alert_dialog.dart';
import 'package:sustain_energy/Utils/show_flushbar.dart';
import 'package:sustain_energy/Utils/show_gradient_app_bar.dart';
import 'package:sustain_energy/bloc/api_service_bloc.dart';
import 'package:sustain_energy/bloc/api_service_event.dart';
import 'package:sustain_energy/bloc/bloc.dart';

class AddEquipment extends StatefulWidget {
  final EquipmentType equipmentType;
  final String equipmentTypeId;
  final String facilityLabel;
  final UserRepository userRepository;
  final String facilityId;

  AddEquipment(
      {@required this.equipmentType,
      @required this.facilityLabel,
      @required this.equipmentTypeId,
      @required this.userRepository,
      @required this.facilityId});

  @override
  _AddEquipmentState createState() => _AddEquipmentState();
}

class NewTypeModel {
  String areaServing;
  FacilityTypeModel type;
  int lightsCount;

  NewTypeModel(
      {@required this.areaServing,
      @required this.type,
      @required this.lightsCount});
}

class TextFieldModel {
  final String label;
  final IconData iconData;
  final TextInputType type;
  final FocusNode currentFocusNode;
  final FocusNode nextFocusNode;
  bool hasError;
  String errorMessage;

  TextFieldModel(
      {@required this.iconData,
      @required this.label,
      @required this.type,
      @required this.currentFocusNode,
      @required this.nextFocusNode,
      @required this.errorMessage,
      @required this.hasError});
}

FocusNode _title = new FocusNode();
FocusNode _areaServing = new FocusNode();
FocusNode _serialNumber = new FocusNode();
FocusNode _lightsCount = new FocusNode();
FocusNode _input = new FocusNode();
FocusNode _output = new FocusNode();
FocusNode _efficiency = new FocusNode();
FocusNode _manufacturer = new FocusNode();
FocusNode _recoveryGate = new FocusNode();
FocusNode _capacity = new FocusNode();
FocusNode _rpm = new FocusNode();
FocusNode _horsePower = new FocusNode();

List<FocusNode> _focusNodesAvailable = [
  _title,
  _areaServing,
  _serialNumber,
  _lightsCount,
  _input,
  _output,
  _efficiency,
  _manufacturer,
  _recoveryGate,
  _capacity,
  _rpm,
  _horsePower
];

List<String> _unAvoidableLabelList = [
  'FieldsAvailble.TITLE',
  'FieldsAvailble.AREA_SERVING',
  'FieldsAvailble.SERIAL_NUMBER',
  'FieldsAvailble.LIGHTS_COUNT',
  'FieldsAvailble.INPUT',
  'FieldsAvailble.OUTPUT',
  'FieldsAvailble.EFFICIENCY',
  'FieldsAvailble.MANUFACTURER',
  'FieldsAvailble.RECOVERY_RATE_GPM',
  'FieldsAvailble.CAPACITY_IN_LTR',
  'FieldsAvailble.RPM',
  'FieldsAvailble.POWER_AKA_HORSE_POWER'
];

class _AddEquipmentState extends State<AddEquipment>
    with SingleTickerProviderStateMixin {
  GlobalKey<ScaffoldState> _scaffoldState = new GlobalKey<ScaffoldState>();

  List<FieldsAvailble> furnaceFields = [
    FieldsAvailble.TITLE,
    FieldsAvailble.INPUT,
    FieldsAvailble.OUTPUT,
    FieldsAvailble.EFFICIENCY,
    FieldsAvailble.SERIAL_NUMBER,
    FieldsAvailble.MANUFACTURER,
    FieldsAvailble.AREA_SERVING
  ];

  List<FieldsAvailble> boilerFields = [
    FieldsAvailble.TITLE,
    FieldsAvailble.INPUT,
    FieldsAvailble.OUTPUT,
    FieldsAvailble.EFFICIENCY,
    FieldsAvailble.AREA_SERVING,
    FieldsAvailble.MANUFACTURER,
    FieldsAvailble.SERIAL_NUMBER,
  ];

  List<FieldsAvailble> overHeadHeatersFields = [
    FieldsAvailble.TITLE,
    FieldsAvailble.INPUT,
    FieldsAvailble.OUTPUT,
    FieldsAvailble.EFFICIENCY,
    FieldsAvailble.SERIAL_NUMBER,
    FieldsAvailble.MANUFACTURER,
    FieldsAvailble.AREA_SERVING
  ];

  List<FieldsAvailble> lightFields = [
    FieldsAvailble.TITLE,
    FieldsAvailble.AREA_SERVING,
    FieldsAvailble.LIGHTS_COUNT,
    FieldsAvailble.TYPE
  ];

  List<FieldsAvailble> motorFields = [
    FieldsAvailble.TITLE,
    FieldsAvailble.POWER_AKA_HORSE_POWER,
    FieldsAvailble.RPM,
    FieldsAvailble.EFFICIENCY,
    FieldsAvailble.SERIAL_NUMBER,
    FieldsAvailble.MANUFACTURER
  ];

  List<FieldsAvailble> pumpFields = [
    FieldsAvailble.TITLE,
    FieldsAvailble.POWER_AKA_HORSE_POWER,
    FieldsAvailble.EFFICIENCY,
    FieldsAvailble.SERIAL_NUMBER,
    FieldsAvailble.MANUFACTURER,
    FieldsAvailble.VARIABLE_FREQUENCY_DRIVE_EQUIPPED
  ];

  List<FieldsAvailble> domesticHotWaterFields = [
    FieldsAvailble.TITLE,
    FieldsAvailble.RECOVERY_RATE_GPM,
    FieldsAvailble.CAPACITY_IN_LTR,
    FieldsAvailble.EFFICIENCY,
    FieldsAvailble.SERIAL_NUMBER,
    FieldsAvailble.MANUFACTURER
  ];

  List<FieldsAvailble> chillerFields = [
    FieldsAvailble.TITLE,
    FieldsAvailble.INPUT,
    FieldsAvailble.OUTPUT,
    FieldsAvailble.EFFICIENCY,
    FieldsAvailble.SERIAL_NUMBER,
    FieldsAvailble.MANUFACTURER
  ];

  Map<String, dynamic> map;
  Map<String, dynamic> properties;
  bool _hasErrorInTE = false;

  Widget _getAllCompatibleFields(EquipmentType type) {
    List<FieldsAvailble> compatibleList = getCompatibleList(type);
    List<Widget> listOfWidgets = [];
    listOfWidgets.clear();
    compatibleList.forEach((f) {
      listOfWidgets.add(
        f == (FieldsAvailble.TITLE)
            ? getTextField('Title', Icon(Icons.title), FieldsAvailble.TITLE,
                isKeyBoardTypeInt: false)
            : f == (FieldsAvailble.INPUT)
                ? getTextField(
                    'BTU/h Input (INT)', Icon(Icons.input), FieldsAvailble.INPUT,
                    isKeyBoardTypeInt: true)
                : f == (FieldsAvailble.OUTPUT)
                    ? getTextField('BTU/h Output (INT)',
                        Icon(Icons.label_outline), FieldsAvailble.OUTPUT,
                        isKeyBoardTypeInt: true)
                    : f == (FieldsAvailble.EFFICIENCY)
                        ? getTextField('Efficiency (Percentage)',
                            Icon(Icons.av_timer), FieldsAvailble.EFFICIENCY,
                            isKeyBoardTypeInt: true)
                        : f == (FieldsAvailble.AREA_SERVING)
                            ? getTextField(
                                'Area Serving',
                                Icon(Icons.select_all),
                                FieldsAvailble.AREA_SERVING,
                                isKeyBoardTypeInt: true,
                              )
                            : f == (FieldsAvailble.CAPACITY_IN_LTR)
                                ? getTextField(
                                    'Capacity (LTR)',
                                    Icon(Icons.all_out),
                                    FieldsAvailble.CAPACITY_IN_LTR,
                                    isKeyBoardTypeInt: true,
                                  )
                                : f == (FieldsAvailble.MANUFACTURER)
                                    ? getTextField(
                                        'Manufacturer',
                                        Icon(OMIcons.business),
                                        FieldsAvailble.MANUFACTURER)
                                    : f ==
                                            (FieldsAvailble
                                                .POWER_AKA_HORSE_POWER)
                                        ? getTextField(
                                            'Horse Power',
                                            Icon(Icons.offline_bolt),
                                            FieldsAvailble
                                                .POWER_AKA_HORSE_POWER,
                                            isKeyBoardTypeInt: true,
                                          )
                                        : f ==
                                                (FieldsAvailble
                                                    .RECOVERY_RATE_GPM)
                                            ? getTextField(
                                                'Recovery Rate (GPM)',
                                                Icon(Icons.timer),
                                                FieldsAvailble
                                                    .RECOVERY_RATE_GPM,
                                                isKeyBoardTypeInt: true,
                                              )
                                            : f == (FieldsAvailble.RPM)
                                                ? getTextField(
                                                    'RPM',
                                                    Icon(Icons.network_check),
                                                    FieldsAvailble.RPM,
                                                    isKeyBoardTypeInt: true,
                                                  )
                                                : f ==
                                                        (FieldsAvailble
                                                            .SERIAL_NUMBER)
                                                    ? getTextField(
                                                        'Serial Number',
                                                        Icon(OMIcons
                                                            .confirmationNumber),
                                                        FieldsAvailble
                                                            .SERIAL_NUMBER)
                                                    : f ==
                                                            (FieldsAvailble
                                                                .VARIABLE_FREQUENCY_DRIVE_EQUIPPED)
                                                        ? _buildBooleanField(
                                                            'Variable Freq Drive Equipped', true)
                                                        : f ==
                                                                (FieldsAvailble
                                                                    .LIGHTS_COUNT)
                                                            ? getTextField(
                                                                'Lights Count',
                                                                Icon(Icons
                                                                    .lightbulb_outline),
                                                                FieldsAvailble
                                                                    .LIGHTS_COUNT,
                                                                isKeyBoardTypeInt:
                                                                    true,
                                                              )
                                                            : f == (FieldsAvailble.TYPE)
                                                                ? buildType(context)
                                                                : Container(),
      );
    });
    return ListView(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        children: listOfWidgets);
  }

  AnimationController controller;

  List<FieldsAvailble> getCompatibleList(EquipmentType type) {
    List compatibleList = type == EquipmentType.FURNACE
        ? furnaceFields
        : type == EquipmentType.BOILER
            ? boilerFields
            : type == EquipmentType.PUMP
                ? pumpFields
                : type == EquipmentType.MOTOR
                    ? motorFields
                    : type == EquipmentType.OVERHEAD_HEATER
                        ? overHeadHeatersFields
                        : type == EquipmentType.DOMESTIC_HOT_WATER
                            ? domesticHotWaterFields
                            : type == EquipmentType.LIGHT
                                ? lightFields
                                : chillerFields;
    return compatibleList;
  }

  List<FocusNode> getCurrentNextFocusNode(
      EquipmentType type, FieldsAvailble textFieldsType) {
    List<FieldsAvailble> compatibleList = getCompatibleList(type);
    FocusNode _currentFocusNode;
    FocusNode _nextFocusNode;
    int currentIndex;
    for (var f in compatibleList) {
      if (f == textFieldsType) {
        for (var item in _unAvoidableLabelList) {
          var itemName = item
              .toString()
              .replaceAll(new RegExp(r'[_]+'), '')
              .trim()
              .toLowerCase();
          var seq = f.toString();
          seq = seq.replaceAll(new RegExp(r'[_]+'), '').trim().toLowerCase();
          _currentFocusNode = seq == itemName
              ? _focusNodesAvailable[_unAvoidableLabelList.indexOf(item)]
              : null;
          if (_currentFocusNode != null) {
            currentIndex = compatibleList.indexOf(f);
            break;
          }
        }
      }
    }
    if (_currentFocusNode != null) {
      _nextFocusNode = (compatibleList.length) > (currentIndex + 1)
          ? getNextFocusNode(compatibleList[currentIndex + 1])
          : null;
      return [_currentFocusNode, _nextFocusNode];
    } else {
      return [null, null];
    }
  }

  FocusNode getNextFocusNode(FieldsAvailble compatibleList) {
    var _nextFocusNode;
    for (var item in _unAvoidableLabelList) {
      var itemName = item
          .toString()
          .replaceAll(new RegExp(r'[_]+'), '')
          .trim()
          .toLowerCase();
      var seq = compatibleList.toString();
      seq = seq.replaceAll(new RegExp(r'[_]+'), '').trim().toLowerCase();
      _nextFocusNode = seq.toLowerCase() == (itemName)
          ? _focusNodesAvailable[_unAvoidableLabelList.indexOf(item)]
          : null;
      if (_nextFocusNode != null) {
        break;
      }
    }
    return _nextFocusNode;
  }

  @override
  void initState() {
    onChange = false;

    map = new Map<String, dynamic>();
    properties = new Map<String, dynamic>();
    map['type'] = widget.equipmentTypeId;
    map['facility'] = widget.facilityId;
    controller = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this);
    _typeModel =
        new NewTypeModel(areaServing: null, lightsCount: null, type: null);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _buildDialog(
          context,
          "Note",
          'Please capture Equipment image in potrait mode only.',
          widget.equipmentType);
    });
    super.initState();
  }

  @override
  void dispose() {
    selectedTypeIndex = 0;
    tempSelectedTypeIndex = 0;
    _controller.dispose();
    super.dispose();
  }

  bool _autoValidate = false;
  bool _hasError = false;
  bool _hasVariableFrequencyDrive = true;
  File mFile_1;
  File mFile_2;

  FacilityTypeModel selectedType;
  FacilityTypeModel tempSelectedType;
  int selectedTypeIndex;
  int tempSelectedTypeIndex;
  bool onChange;

  NewTypeModel _typeModel;

  var _controller = FixedExtentScrollController(initialItem: 0);

  Future _buildDialog(BuildContext context, String title, String message,
      EquipmentType type) async {
    ShowAlertDialog(
      title: title,
      message: message,
      type: DialogTypes.WITH_CAMERA_GALLERY,
      isDismissable: true,
      positiveButtonLabel: null,
      negativeButtonLabel: null,
      positiveFunctionality: () {
        firstDialogPressed(title, type, true);
      },
      negativeFunctionality: () {
        firstDialogPressed(title, type, false);
      },
    ).show(context);
  }

  Future firstDialogPressed(
      String title, EquipmentType type, bool fromCamera) async {
    if (fromCamera) {
      // Obtain a list of the available cameras on the device.
      final cameras = await availableCameras();

      // Get a specific camera from the list of available cameras.
      final firstCamera = cameras.first;

      //await ImagePicker.pickImage(source: ImageSource.camera);
      var mPath =
          await Navigator.push(context, CupertinoPageRoute(builder: (context) {
        return TakePictureScreen(
          camera: firstCamera,
        );
      }));

      if (mPath != null && mPath != "") {
        _buildDialog2(
            context,
            title,
            'Look for the nameplate on the system, usually it isusually located underneath the front cover of the furnace, or on the front or side of the furnace \n\n This photo should be taken in landscape mode.',
            type,
            File(mPath.toString()));
      } else {
        Navigator.pop(context);
        _scaffoldState.currentState.showSnackBar(SnackBar(
          content:
              Text('You can still add pictures by tapping on camera icon.'),
        ));
      }
    } else {
      var mPath = await ImagePicker.pickImage(
          source: ImageSource.gallery, imageQuality: 95);
      if (mPath != null) {
        _buildDialog2(
            context,
            title,
            'Look for the nameplate on the system, usually it isusually located underneath the front cover of the furnace, or on the front or side of the furnace \n\n This photo should be taken in landscape mode.',
            type,
            mPath);
      } else {
        Navigator.pop(context);
        _scaffoldState.currentState.showSnackBar(SnackBar(
          content:
              Text('You can still add pictures by tapping on camera icon.'),
        ));
      }
    }
  }

  Future _buildDialog2(BuildContext context, String title, String message,
      EquipmentType type, File path_1) async {
    Navigator.pop(context);
    ShowAlertDialog(
      title: title,
      message: message,
      type: DialogTypes.WITH_CAMERA_GALLERY,
      isDismissable: true,
      positiveButtonLabel: null,
      negativeButtonLabel: null,
      positiveFunctionality: () {
        secondDialogPressed(type, path_1, true);
      },
      negativeFunctionality: () {
        secondDialogPressed(type, path_1, false);
      },
    ).show(context);
  }

  Future<void> secondDialogPressed(
      EquipmentType type, File path_1, bool fromCamera) async {
    CircularProgressIndicator();
    if (fromCamera) {
      // Obtain a list of the available cameras on the device.
      final cameras = await availableCameras();

      // Get a specific camera from the list of available cameras.
      final firstCamera = cameras.first;

      //await ImagePicker.pickImage(source: ImageSource.camera);
      var path_2 =
          await Navigator.push(context, CupertinoPageRoute(builder: (context) {
        return TakePictureScreen(
          camera: firstCamera,
        );
      }));

      if (path_2 != null && path_2 != "") {
        Navigator.pop(context);
        mFile_1 = path_1;
        mFile_2 = File(path_2);
        setState(() {});
      } else {
        Navigator.pop(context);
        _scaffoldState.currentState.showSnackBar(SnackBar(
          content:
              Text('You can still add pictures by tapping on camera icon.'),
        ));
      }
    } else {
      var path_2 = await ImagePicker.pickImage(
          source: ImageSource.gallery, imageQuality: 95);

      if (path_2 != null) {
        Navigator.pop(context);
        mFile_1 = path_1;
        mFile_2 = path_2;
        setState(() {});
      } else {
        Navigator.pop(context);
        _scaffoldState.currentState.showSnackBar(SnackBar(
          content:
              Text('You can still add pictures by tapping on camera icon.'),
        ));
      }
    }
  }

  GlobalKey<FormState> _globalKey = new GlobalKey<FormState>();

  /// ## Has Error At Indexed
  /// Here index points to photo 1 or photo 2
  /// if index = 0 error at photo 1 and index = 1 error at photo 2
  /// else index = 2 error at both photo 1 & 2, index = 3 no error.
  int _hasErrorIndex = 3;

  @override
  Widget build(BuildContext context) {
    final Animation<double> offsetAnimation = Tween(begin: 0.0, end: 24.0)
        .chain(CurveTween(curve: Curves.elasticIn))
        .animate(controller)
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              controller.reverse();
            }
          });
    String title = widget.equipmentType.toString();
    title = title.substring(title.indexOf('.'), title.length);
    title = title.replaceAll(new RegExp(r'[.]+'), '');
    title = title.replaceAll(new RegExp(r'[_]+'), ' ');
    title = title.toLowerCase();
    title = title[0].toUpperCase() + title.substring(1);
    //${s[0].toUpperCase()}${s.substring(1)}'

    return Scaffold(
      key: _scaffoldState,
      appBar: ShowGradientAppBar(
              title: 'Add $title',
              leadingIcon: OMIcons.arrowBackIos,
              leadIconClickFuncitonality: null,
              actions: null)
          .show(context),
      body: BlocListener<ApiServiceBloc, ApiServiceState>(
        listener: (context, state) {
          if (state is DataLoadingFailed) {
            ShowFlushBar(
                    duration: 5,
                    context: context,
                    title: 'Data Loading Failure',
                    message: state.error.contains('SocketException')
                        ? 'You\'re not connected to Internet, please try again after connection established'
                        : '${state.error}',
                    isInProgress: false,
                    isThisError: true,
                    isSuccess: false)
                .show();
          }
          if (state is DataLoading) {
            //buildLoadingWidget(context);
            print('LOADING');
          }
          if (state is DataLoaded) {
            // showSuccessMsg(context);
            if (state.model is AddEquipmentResponse) {
              BlocProvider.of<ApiServiceBloc>(context).add(AddPhotos(
                  id: (state.model as AddEquipmentResponse).id,
                  photoPath: mFile_1.path,
                  nameplatePath: mFile_2.path));
            } else if (state.model is List<Response>) {
              Navigator.pop(context);
              Navigator.pop(context, true);
            }
          }
        },
        child: Form(
          autovalidate: _autoValidate,
          key: _globalKey,
          child: ListView(
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            children: <Widget>[
              CustomTopContainer(),
              SizedBox(
                height: 20,
              ),
              _getAllCompatibleFields(widget.equipmentType),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  buildPicutreFrame(
                      context,
                      'Picture Of ${widget.equipmentType == EquipmentType.BOILER ? 'Boiler' : widget.equipmentType == EquipmentType.CHILLER ? 'Chiller' : widget.equipmentType == EquipmentType.DOMESTIC_HOT_WATER ? 'hot water tank' : widget.equipmentType == EquipmentType.FURNACE ? 'furnance' : widget.equipmentType == EquipmentType.LIGHT ? 'light' : widget.equipmentType == EquipmentType.MOTOR ? 'motor' : 'pump'}',
                      0,
                      offsetAnimation),
                  buildPicutreFrame(
                      context, 'Picture Of Nameplate', 1, offsetAnimation)
                ],
              ),
              SizedBox(
                height: 20,
              ),
              buildContainerMsg(),
              buildButton(),
              SizedBox(
                height: 100,
              )
            ],
          ),
        ),
      ),
    );
  }

  void showSuccessMsg(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => ShowFlushBar(
            context: context,
            duration: 5,
            title: 'Data has been loaded successfully.',
            message: 'We appreciate your patience.',
            isInProgress: false,
            isThisError: false,
            isSuccess: true)
        .show());
  }

  Center buildLoadingWidget(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => ShowFlushBar(
            context: context,
            title: 'Data is Loading',
            message: 'Please be patient...',
            duration: null,
            isInProgress: true,
            isThisError: false,
            isSuccess: false)
        .show());
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Container buildContainerMsg() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      decoration: BoxDecoration(
          color: Colors.lightBlue.withOpacity(0.3),
          borderRadius: BorderRadius.circular(5)),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: Text(
            '*Equipemnt Photo must be taken in potrait mode only & Nameplate photo must be taken in landscape mode only.',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                color: Colors.black.withOpacity(0.7)),
          ),
        ),
      ),
    );
  }

  Container buildButton() {
    return Container(
      margin: EdgeInsets.fromLTRB(80, 20, 80, 8),
      child: RaisedButton(
        padding: EdgeInsets.all(0),
        textColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80)),
        onPressed: () {
          if (_globalKey.currentState.validate()) {
            setState(() {
              _hasError = validatePhoto();
            });
            if (!_hasError) {
              if (widget.equipmentType == EquipmentType.PUMP) {
                properties['has_variable_frequency_drive'] =
                    _hasVariableFrequencyDrive;
              } else if (widget.equipmentType == EquipmentType.LIGHT) {
                properties['type'] = selectedType.name;
              }
              bool _isFinalized =
                  mFile_1.lengthSync() < Consts.imageAcceptedSize &&
                      mFile_2.lengthSync() < Consts.imageAcceptedSize;
              if (_isFinalized) {
                map['properties'] = properties;
                print(map.toString());
                BlocProvider.of<ApiServiceBloc>(context)
                    .add(PostEquipment(body: map));
                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (context) {
                      return Dialog(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6),
                        ),
                        elevation: 0.0,
                        backgroundColor: Colors.transparent,
                        child: MyDialogContent(),
                      );
                    });
              } else {
                ShowFlushBar(
                        context: context,
                        duration: 5,
                        isInProgress: false,
                        isSuccess: false,
                        isThisError: true,
                        title: 'Image size is too large than we accepted!',
                        message:
                            'Image size should be less than 1 Megabyte, We may increase this in future.')
                    .show();
              }
            }
          } else {
            _autoValidate = true;
          }
          setState(() {
            if (_hasErrorInTE) {
              ShowFlushBar(
                      isInProgress: false,
                      context: context,
                      duration: 3,
                      isSuccess: false,
                      isThisError: true,
                      message: 'Please fill the fields as mentioned.',
                      title: 'Error')
                  .show();
            } else if (_hasError) {
              controller.forward(from: 0.0);
              ShowFlushBar(
                      isInProgress: false,
                      context: context,
                      duration: 3,
                      isSuccess: false,
                      isThisError: true,
                      message: 'Please upload the photos.',
                      title: 'Error')
                  .show();
            }
            _autoValidate = true;
          });
        },
        child: Container(
          padding: EdgeInsets.all(12),
          child: Center(
              child: Text(
            "Submit",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
          )),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(80),
              gradient: LinearGradient(colors: [
                Color(0xff53d8d5),
                Color(0xff52a9e9),
              ])),
        ),
      ),
    );
  }

  Widget buildPicutreFrame(BuildContext context, String label, int identity,
      Animation<double> offsetAnimation) {
    return AnimatedBuilder(
        animation: offsetAnimation,
        builder: (buildContext, child) {
          if (offsetAnimation.value < 0.0)
            print('${offsetAnimation.value + 8.0}');
          return Container(
            margin: EdgeInsets.symmetric(horizontal: 0.0),
            padding: EdgeInsets.only(
                left: offsetAnimation.value + 12.0,
                right: 12.0 - offsetAnimation.value),
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () async {
                    _showBottomSheet(context, identity);
                  },
                  borderRadius: BorderRadius.circular(5),
                  child: Stack(
                    alignment: Alignment.bottomRight,
                    children: <Widget>[
                      DottedBorder(
                        borderType: BorderType.RRect,
                        strokeWidth: 1.5,
                        dashPattern: [9, 5],
                        color: _hasError
                            ? identity == _hasErrorIndex || _hasErrorIndex == 2
                                ? Colors.red
                                : Colors.grey.withOpacity(0.5)
                            : Colors.grey.withOpacity(0.5),
                        radius: Radius.circular(5),
                        padding:
                            EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                        child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.grey.withOpacity(0.5)),
                            width: MediaQuery.of(context).size.width / 2.5,
                            height: 160,
                            child: identity == 0
                                ? getIconForDottedImage(mFile_1)
                                : getIconForDottedImage(mFile_2)),
                      ),
                      identity == 1
                          ? InkWell(
                              onTap: () {
                                ShowFlushBar(
                                        context: context,
                                        duration: 3,
                                        isInProgress: false,
                                        isSuccess: false,
                                        isThisError: false,
                                        message:
                                            'Look for the nameplate on the system, usually it located underneath the front cover of the ${widget.equipmentType == EquipmentType.BOILER ? 'Boiler' : widget.equipmentType == EquipmentType.CHILLER ? 'Chiller' : widget.equipmentType == EquipmentType.DOMESTIC_HOT_WATER ? 'hot water tank' : widget.equipmentType == EquipmentType.FURNACE ? 'furnance' : widget.equipmentType == EquipmentType.LIGHT ? 'light' : widget.equipmentType == EquipmentType.MOTOR ? 'motor' : 'pump'}, or on the front or side of the ${widget.equipmentType == EquipmentType.BOILER ? 'Boiler' : widget.equipmentType == EquipmentType.CHILLER ? 'Chiller' : widget.equipmentType == EquipmentType.DOMESTIC_HOT_WATER ? 'hot water tank' : widget.equipmentType == EquipmentType.FURNACE ? 'furnance' : widget.equipmentType == EquipmentType.LIGHT ? 'light' : widget.equipmentType == EquipmentType.MOTOR ? 'motor' : 'pump'}',
                                        title: 'Info')
                                    .show();
                              },
                              borderRadius: BorderRadius.circular(30),
                              radius: 30.0,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Tooltip(
                                  message: 'Where can I found nameplate?',
                                  child: Icon(
                                    Icons.help_outline,
                                    size: 18,
                                    color: Colors.blue,
                                  ),
                                ),
                              ),
                            )
                          : Container()
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Center(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      label,
                      style: TextStyle(
                          color: Colors.grey, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                  ],
                ))
              ],
            ),
          );
        });
  }

  Widget getIconForDottedImage(File mFile) {
    return mFile == null
        ? Icon(
            Icons.camera_alt,
            size: 50,
            color: Colors.grey,
          )
        : ClipRRect(
            child: Image.file(
              File(mFile.path),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.circular(5),
          );
  }

  void changeType(int index, List<FacilityTypeModel> list) {
    onChange = true;
    tempSelectedTypeIndex = index;
    tempSelectedType =
        new FacilityTypeModel(id: list[index].id, name: list[index].name);
  }

  void _changeFocus(FocusNode currentFocusNode, FocusNode nextFocusNode) {
    currentFocusNode.unfocus();
    nextFocusNode.requestFocus();
    setState(() {});
  }

  Padding buildType(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(15),
      child: InkWell(
        onTap: () {
          _buildBottomSheet(context, _controller);
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5), topRight: Radius.circular(5)),
            color: Colors.grey[200],
          ),
          child: TextFormField(
            validator: (arg) {
              if (arg == 'Facility Type') {
                return 'Please select valid type';
              } else {
                return null;
              }
            },
            enabled: false,
            cursorColor: Colors.blue,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(10),
                focusColor: Colors.teal,
                labelText: selectedType == null ? 'Type' : selectedType.name,
                labelStyle: TextStyle(
                  fontWeight: FontWeight.w600,
                ),
                suffixIcon: Icon(Icons.keyboard_arrow_down)),
          ),
        ),
      ),
    );
  }

  void _showBottomSheet(BuildContext mContext, int identity) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10))),
        context: mContext,
        builder: (context) {
          return ListView(
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(10),
                child: Center(
                  child: Text(
                    'Select picture from',
                    style: TextStyle(
                        color: Colors.grey.withOpacity(0.5),
                        fontSize: 20,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.camera_alt),
                title: Text('Camera',
                    style: TextStyle(fontWeight: FontWeight.w600)),
                onTap: () async {
                  Navigator.pop(context);
                  // Obtain a list of the available cameras on the device.
                  final cameras = await availableCameras();
                  // Get a specific camera from the list of available cameras.
                  final firstCamera = cameras.first;
                  String mPath = await Navigator.push(context,
                      CupertinoPageRoute(builder: (context) {
                    return TakePictureScreen(
                      // Pass the appropriate camera to the TakePictureScreen widget.
                      camera: firstCamera,
                    );
                  }));
                  if (mPath != "") {
                    identity == 0
                        ? mFile_1 = File(mPath)
                        : mFile_2 = File(mPath);
                    setState(() {});
                  }
                  // var mPath =
                  //     await ImagePicker.pickImage(source: ImageSource.camera);
                  // if (mPath.path != "") {
                  //   identity == 0 ? mFile_1 = mPath : mFile_2 = mPath;
                  //   setState(() {});
                  // }
                },
              ),
              Divider(),
              ListTile(
                leading: Icon(Icons.photo_album),
                title: Text(
                  'Gallery',
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
                onTap: () async {
                  Navigator.pop(context);

                  var mPath = await ImagePicker.pickImage(
                      source: ImageSource.gallery, imageQuality: 95);
                  // var mPath = await Navigator.push(mContext,
                  //     CupertinoPageRoute(builder: (context) {
                  //   return TakePictureScreen(
                  //     camera: firstCamera,
                  //   );
                  // }));

                  if (mPath != null && mPath.path != "") {
                    identity == 0 ? mFile_1 = mPath : mFile_2 = mPath;
                    setState(() {});
                  }
                },
              )
            ],
          );
        });
  }

  Widget getTextField(String label, Icon icon, FieldsAvailble type,
      {bool isKeyBoardTypeInt = false}) {
    var model = getCurrentNextFocusNode(widget.equipmentType, type);
    return Padding(
      padding: EdgeInsets.all(15),
      child: new TextFormField(
        validator: (m) {
          return validate(label, m);
        },
        onChanged: (seq) {
          if (label.toLowerCase().trim().contains('title')) {
            map['name'] = seq;
          } else if (label.toLowerCase().trim().contains('input')) {
            properties['input_btu_h'] = double.parse(seq);
          } else if (label.toLowerCase().trim().contains('output')) {
            properties['output_btu_h'] = double.parse(seq);
          } else if (label.toLowerCase().trim().contains('efficiency')) {
            properties['efficiency_percentage'] = double.parse(seq);
          } else if (label.toLowerCase().trim().contains('manufacturer')) {
            properties['manufacturer'] = seq;
          } else if (label.toLowerCase().trim().contains('recovery')) {
            properties['recovery_rate_gpm'] = double.parse(seq);
          } else if (label.toLowerCase().trim().contains('capacity')) {
            properties['capacity_litres'] = int.parse(seq);
          } else if (label.toLowerCase().trim().contains('rpm')) {
            properties['rpm'] = int.parse(seq);
          } else if (label.toLowerCase().trim().contains('horse power')) {
            properties['power_hp'] = double.parse(seq);
          } else if (label.toLowerCase().trim().contains('area serving')) {
            properties['area_serving'] = double.parse(seq);
          } else if (label.toLowerCase().trim().contains('serial number')) {
            properties['serial_number'] = seq;
          } else if (label.toLowerCase().trim().contains('lights count')) {
            properties['lights_count'] = int.parse(seq);
          }
        },
        onFieldSubmitted: (arg) {
          if (model[1] != null) {
            _changeFocus(model[0], model[1]);
          }
        },
        keyboardType: isKeyBoardTypeInt
            ? TextInputType.numberWithOptions(decimal: true)
            : null,
        textInputAction:
            model[1] == null ? TextInputAction.done : TextInputAction.next,
        focusNode: model[0],
        cursorColor: Colors.blue,
        decoration: InputDecoration(
            filled: true,
            fillColor: Colors.grey[200],
            contentPadding: EdgeInsets.all(10),
            focusColor: Colors.teal,
            labelText: label,
            labelStyle: TextStyle(
              fontWeight: FontWeight.w600,
            ),
            suffixIcon: icon),
      ),
    );
  }

  /// validate the fields based on the conditions.
  String validate(String label, String seq) {
    if (seq.isEmpty) {
      _hasErrorInTE = true;
      return 'Please enter the $label.';
    } /*else if (seq.length <= 0) {
      _hasErrorInTE = true;
      return '$label length should be greater than 3';
    }*/ else {
      _hasErrorInTE = false;
      return null;
    }
  }

  bool validatePhoto() {
    if (mFile_1 == null || mFile_2 == null) {
      _hasError = true;
      mFile_1 == null && mFile_2 == null
          ? _hasErrorIndex = 2
          : mFile_1 == null ? _hasErrorIndex = 0 : _hasErrorIndex = 1;
    } else {
      mFile_1.path == ""
          ? _hasError = true
          : mFile_2.path == "" ? _hasError = true : _hasError = false;
    }
    return _hasError;
  }

  _buildBottomSheet(
      BuildContext context, FixedExtentScrollController movePointerToPosition) {
    onChange = false;
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10))),
        context: context,
        builder: (context) {
          var data = List.generate(10, (index) {
            return new FacilityTypeModel(
                id: '$index', name: 'Light Type $index');
          });
          var container = buildStaticBottomSheet(context, data);
          return container;
        });
  }

  Container buildStaticBottomSheet(BuildContext context, data) {
    return Container(
      height: 280,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 10.0, left: 20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Select Type',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      color: Colors.grey.withOpacity(0.5)),
                ),
                IconButton(
                  icon: Icon(Icons.done, color: Colors.green),
                  onPressed: () {
                    Navigator.pop(context);

                    // There are total of 3 possibilites:
                    // 1. Opened bottom sheet and closed with out selection, in this case we don't do anything.
                    // 2. Opened bottom sheet and closed with out selection, but changed the type (I mean user
                    // scrolled the list but not pressed done icon)
                    // In case 2 as user scrolled our Cupertino build picker triggers the OnSelectionChanged method which will affect the following fields:
                    // * onChange = true
                    // ** tempSelectedIndex = index;
                    // *** tempSelectedFacility = list[index].id & list[index].name
                    // 3. Opened bottom sheet and closed with selection.

                    // 620-628 code is responsible for the following points.
                    // -> It will avoid any selection that user was not intended. (like user scrolled the list but not selected.)
                    // In above case we will ignore the temp values and override them once the icon btn pressed, override with that respective data.
                    // -> It will avoid the scrolling the list to unselected item (normally our list automatically scrolls to previously selected item).

                    if (onChange) {
                      selectedTypeIndex = tempSelectedTypeIndex;
                      _typeModel.type = selectedType = tempSelectedType;
                      if (_typeModel.type == null) {
                        _typeModel.type = selectedType = new FacilityTypeModel(
                            id: data[selectedTypeIndex].id,
                            name: data[selectedTypeIndex].value);
                      }
                    } else {
                      _typeModel.type = selectedType = new FacilityTypeModel(
                          id: data[0].id, name: data[0].value);
                    }
                    setState(() {});
                  },
                  padding: EdgeInsets.only(right: 10.0),
                  tooltip: 'Done',
                )
              ],
            ),
          ),
          BuildCupertinoPicker(
            index: selectedTypeIndex,
            list: data,
            callback: changeType,
          )
        ],
      ),
    );
  }

  Widget _buildBooleanField(String s, bool param1) {
    return Padding(
      padding: EdgeInsets.all(15),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5), topRight: Radius.circular(5)),
          color: Colors.grey[200],
        ),
        child: SwitchListTile.adaptive(
          onChanged: (value) {
            _hasVariableFrequencyDrive = value;
          },
          value: param1,
          activeColor: Colors.blue,
          title: Text(
            s,
            style: TextStyle(
                color: Colors.black.withOpacity(0.6),
                fontWeight: FontWeight.w600),
          ),
        ),
      ),
    );
  }
}

class MyDialogContent extends StatefulWidget {
  @override
  _MyDialogContentState createState() => _MyDialogContentState();
}

class _MyDialogContentState extends State<MyDialogContent> {
  String message;
  String title;
  bool isLoading;

  @override
  void initState() {
    message = '';
    title = '';
    isLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width - 80;
    return BlocListener<UploadingBloc, UploadingState>(
        listener: (context, state) {
      if (state is Uploading) {
        isLoading = true;
        title = state.type == TypeUploaded.CREATED ? 'Creating' : 'Uploading';
        message = state.type == TypeUploaded.CREATED
            ? 'Please be patient while we create your equipment...'
            : 'Please be patient while we uploading your equipment photos \n\nUploading ${state.percentage}%';
      } else if (state is Uploaded) {
        isLoading = false;
        title = state.type == TypeUploaded.CREATED ? 'Uploading' : 'Loading';
        message = state.type == TypeUploaded.CREATED
            ? 'Please be patient while we uploading your equipment photos \n\nUploading ${0}%'
            : 'Successfully created equipment\ntaking you back to main screen';
      } else if (state is UploadingFailed) {
        isLoading = false;
        title = 'Failed';
        message = state.typeUploaded == TypeUploaded.CREATED
            ? 'We\'re facing trouble to create your equipment\nplease try agian later'
            : 'We\'re facing trouble to upload photos,\nplease try again with different photo';
      }
      setState(() {});
    }, child: BlocBuilder<UploadingBloc, UploadingState>(
      builder: (context, state) {
        if (state is Uploading) {
          isLoading = true;
          title = state.type == TypeUploaded.CREATED ? 'Creating' : 'Uploading';
          message = state.type == TypeUploaded.CREATED
              ? 'Please be patient while we create your equipment...'
              : 'Please be patient while we uploading your equipment photos \n\nUploading ${state.percentage}%';
        } else if (state is Uploaded) {
          isLoading = false;
          title = state.type == TypeUploaded.CREATED ? 'Uploading' : 'Loading';
          message = state.type == TypeUploaded.CREATED
              ? 'Please be patient while we uploading your equipment photos \n\nUploading ${0}%'
              : 'Successfully created equipment\ntaking you back to main screen';
        } else if (state is UploadingFailed) {
          isLoading = false;
          title = 'Failed';
          message = state.typeUploaded == TypeUploaded.CREATED
              ? 'We\'re facing trouble to create your equipment\nplease try agian later \n\n${state.error}'
              : 'We\'re facing trouble to upload photos,\nplease try again with different photo\n\n${state.error}';
          WidgetsBinding.instance.addPostFrameCallback((callback) {
            setState(() {});
          });
        }
        return Material(
          type: MaterialType.transparency,
          child: InkWell(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: () {},
              child: Stack(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 10.0,
                          offset: const Offset(0.0, 10.0),
                        ),
                      ],
                    ),
                    margin: EdgeInsets.only(top: Consts.avatarRadius),
                    child: CustomPaint(
                      painter: CustomCut(),
                      child: Container(
                        padding: EdgeInsets.only(
                          top: 8 + Consts.padding,
                          bottom: Consts.padding,
                          left: Consts.padding,
                          right: Consts.padding,
                        ),
                        decoration: new BoxDecoration(
                          color: Colors.transparent,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(Consts.padding),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          mainAxisSize:
                              MainAxisSize.min, // To make the card compact
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  title ?? '...',
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 50.0),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                message ?? '...',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 16.0,
                                ),
                              ),
                            ),
                            SizedBox(height: 30.0),
                            Align(
                                alignment: Alignment.bottomRight,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    FlatButton(
                                      onPressed: () {
                                        Navigator.of(context)
                                            .pop(); // To close the dialog
                                      },
                                      child: Text('Close'),
                                    )
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                  ),

                  //...top circlular image part,
                  Positioned(
                    top: Consts.padding + 10,
                    right: width / 10,
                    child: Stack(
                      children: <Widget>[
                        Visibility(
                          child: SizedBox(
                            width: 70,
                            height: 70,
                            child: CircularProgressIndicator(
                              strokeWidth: 6,
                              valueColor: AlwaysStoppedAnimation(
                                  Theme.of(context).primaryColor),
                            ),
                          ),
                          visible: isLoading != null && isLoading,
                        ),
                        Material(
                          elevation: 8,
                          clipBehavior: Clip.antiAlias,
                          shape: CircleBorder(),
                          child: CircleAvatar(
                            maxRadius: 35,
                            backgroundColor: Colors.white,
                            child: Image.asset(
                              'assets/images/app_icon.png',
                              scale: 3.5,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
        );
      },
    ));
  }
}
