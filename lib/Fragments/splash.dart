import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import 'package:sustain_energy/bloc/authentication_bloc.dart';
import 'package:sustain_energy/bloc/bloc.dart';

class Splash extends StatefulWidget {
  final UserRepository userRepository;

  Splash({@required this.userRepository});

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 2500), () async {
      try {
        final bool hasToken = await widget.userRepository.hasToken();
        if (hasToken) {
          BlocProvider.of<AuthenticationBloc>(context).add(Authenticate());
        } else {
          BlocProvider.of<AuthenticationBloc>(context).add(UnAuthenticate());
        }
      } catch (e) {
        BlocProvider.of<AuthenticationBloc>(context).add(UnAuthenticate());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      body: new Center(
          child: Stack(
        children: <Widget>[
          Image.asset(
            'assets/images/splash_bg.jpg',
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
          ),
          Center(
              child: Image.asset(
            'assets/images/splash_logo.png',
            fit: BoxFit.cover,
            height: 250,
          ))
        ],
      )),
    );
  }
}
