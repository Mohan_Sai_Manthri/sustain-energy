import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import 'package:sustain_energy/Utils/show_gradient_app_bar.dart';
import 'package:sustain_energy/bloc/bloc.dart';

import 'add_facility_body.dart';

class AddNewFacility extends StatelessWidget {
  final UserRepository userRepository;
  final BuildContext mContext;
  AddNewFacility(
      {Key key, @required this.userRepository, @required this.mContext})
      : assert(userRepository != null),
        assert(mContext != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ShowGradientAppBar(
        title: 'Add New Facility',
        leadingIcon: OMIcons.arrowBackIos,
        leadIconClickFuncitonality: null,
        actions: null,
      ).show(context),
      body: BlocProvider(
        builder: (context) {
          return AddFacilityBloc(
              userRepository: userRepository, context: mContext)
            ..add(Initialize());
        },
        child: AddNewFacilityBody(
          userRepository: userRepository,
        ),
      ),
    );
  }
}
