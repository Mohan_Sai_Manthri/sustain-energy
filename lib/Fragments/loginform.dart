import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:sustain_energy/Utils/show_flushbar.dart';
import 'package:sustain_energy/bloc/bloc.dart';
import 'package:sustain_energy/bloc/login_bloc.dart';
import 'package:sustain_energy/bloc/login_event.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  //Focus Nodes
  // These are used to focus the next text field when user presses the next button in keyboard
  FocusNode _emailFocus = new FocusNode();
  FocusNode _passwordFocus = new FocusNode();

  LoginBloc loginBloc;
  final _emailController = new TextEditingController();
  final _passwordController = new TextEditingController();

  // Holds the value
  String username;
  String password;

  // by default auto validate will be false, but if there is any validation errors then we will make it true so that it will aut validate each and every field on every change.
  bool _autoValidate = false;
  @override
  void initState() {
    loginBloc = BlocProvider.of<LoginBloc>(context);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailure) {
          ShowFlushBar(
                  context: context,
                  duration: null,
                  isInProgress: false,
                  isSuccess: false,
                  isThisError: true,
                  message: state.error.contains('SocketException')
                      ? 'We\'re facing trouble to connect to the Internet'
                      : state.error.contains('401')
                          ? 'Invalid username/password.'
                          : '${state.error}',
                  title: 'Login Failure')
              .show();
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
        return Form(
          key: _formKey,
          child: Scaffold(
              backgroundColor: Colors.white,
              body: Stack(
                children: <Widget>[
                  Image.asset(
                    'assets/images/login_bg.jpg',
                    fit: BoxFit.cover,
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        child: state is LoginLoading
                            ? Center(
                                child: CircularProgressIndicator(),
                              )
                            : null,
                      ),
                      ListView(
                        physics: BouncingScrollPhysics(),
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(bottom: 25, top: 75),
                              child: Image.asset(
                                'assets/images/login_logo.png',
                                height: 250,
                              )),
                          SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: Text(
                              'Login',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.grey.withOpacity(0.6)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20, 5, 20, 10),
                            child: TextFormField(
                              controller: _emailController,
                              autovalidate: _autoValidate,
                              focusNode: _emailFocus,
                              onFieldSubmitted: (val) {
                                _fieldFocusChange(
                                    context, _emailFocus, _passwordFocus);
                              },
                              validator: (arg) {
                                if (arg.isEmpty || !_validateEmail(arg)) {
                                  return ('Please Enter your email address');
                                } else {
                                  return null;
                                }
                              },
                              onSaved: (arg) {
                                username = arg;
                              },
                              keyboardType: TextInputType.emailAddress,
                              textInputAction: TextInputAction.next,
                              cursorColor: Colors.blue,
                              decoration: InputDecoration(
                                  focusColor: Colors.blue,
                                  labelText: 'Username',
                                  labelStyle:
                                      TextStyle(fontWeight: FontWeight.w600),
                                  suffixIcon: Icon(Icons.person_outline)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                            child: TextFormField(
                              controller: _passwordController,
                              autovalidate: _autoValidate,
                              onFieldSubmitted: (arg) {
                                // Submit for authentication
                              },
                              onSaved: (arg) {
                                password = arg;
                              },
                              validator: (arg) {
                                if (arg.isEmpty) {
                                  return 'Please enter the password';
                                } else if (!_validatePassword(arg)) {
                                  return ('Please enter the password in this format example1');
                                } else {
                                  return null;
                                }
                              },
                              focusNode: _passwordFocus,
                              obscureText: true,
                              cursorColor: Colors.blue,
                              decoration: InputDecoration(
                                  labelStyle:
                                      TextStyle(fontWeight: FontWeight.w600),
                                  labelText: 'Password',
                                  suffixIcon: IconButton(
                                    onPressed: () {},
                                    icon: Icon(OMIcons.vpnKey),
                                  )),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(80, 20, 80, 8),
                            child: RaisedButton(
                              padding: EdgeInsets.all(0),
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(80)),
                              onPressed: () {
                                if (state is! LoginLoading) {
                                  _validateInputFields();
                                }
                              },
                              child: Container(
                                padding: EdgeInsets.all(12),
                                child: Center(
                                    child: Text(
                                  "Login",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                )),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(80),
                                    gradient: LinearGradient(colors: [
                                      Color(0xff53d8d5),
                                      Color(0xff52a9e9),
                                    ])),
                              ),
                            ),
                          ),
                          Center(
                            child: FlatButton(
                              onPressed: () {},
                              child: Text(
                                'Forgot your password?',
                                style: TextStyle(
                                    color: Colors.grey.withOpacity(0.5),
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ],
              )),
        );
      }),
    );
  }

  bool _validatePassword(String password) {
    //(?=.*?[!@#\$&*~]) for Special characted
    // (?=.*?[A-Z]) for capital letter
    RegExp reg = new RegExp(r'^(?=.*?[a-z])(?=.*?[0-9]).{8,}$');
    return reg.hasMatch(password);
  }

  bool _validateEmail(String email) {
    RegExp reg = new RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
    return reg.hasMatch(email);
  }

  Future _validateInputFields() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      try {
        await SystemChannels.textInput.invokeMethod('TextInput.hide');
        loginBloc.add(AuthenticateUser(
            username: _emailController.text,
            password: _passwordController.text));
      } catch (e) {
        print(e);
      }
    } else {
      _autoValidate = true;
      setState(() {});
    }

    // Navigator.push(context, CupertinoPageRoute(builder: (context) {
    //   return SelectMainFacility();
    // }));

    // final response = await Provider.of<PostApiService>(context).loginUser(
    //     {'email': 'user1@sustainergy.com', 'password': 'user1sustainergy'});
    // print(response.statusCode);
    // print(response.error);
    // print(response.body);
  }

  void _fieldFocusChange(BuildContext context, FocusNode currentFocusedItem,
      FocusNode nextFocudNode) {
    currentFocusedItem.unfocus();
    FocusScope.of(context).requestFocus(nextFocudNode);
  }
}
