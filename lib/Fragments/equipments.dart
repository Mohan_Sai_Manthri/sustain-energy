import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sustain_energy/Fragments/select_facility.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import 'package:sustain_energy/bloc/bloc.dart';

class EquipmentsPage extends StatelessWidget {
  final UserRepository userRepository;
  final BuildContext mContext;
  final String facilityId;
  final String facilityLabel;
  EquipmentsPage(
      {Key key,
      @required this.facilityLabel,
      @required this.userRepository,
      @required this.mContext,
      @required this.facilityId})
      : assert(userRepository != null),
        assert(mContext != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MultiBlocProvider(
        providers: [
          BlocProvider<GetequipmenttypeBloc>(
            builder: (context) {
              return GetequipmenttypeBloc(
                  userRepository: userRepository, context: mContext)
                ..add(GetEquipmentTypes());
            },
          ),
          BlocProvider<ApiServiceBloc>(
            builder: (context) {
              return ApiServiceBloc(
                  userRepository: userRepository, context: mContext)
                ..add(GetEquipmentResults(page: 1));
            },
          ),
        ],
        child: SelectFacility(
          facilityLabel: facilityLabel,
          userRepository: userRepository,
          facilityId: facilityId,
        ),
      ),
    );
  }
}
