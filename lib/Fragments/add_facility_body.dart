import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sustain_energy/CustomViews/custom_top.dart';
import 'package:sustain_energy/Models/facility_type.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import 'package:sustain_energy/Utils/build_cupertino_picker.dart';
import 'package:sustain_energy/Utils/constant.dart';
import 'package:sustain_energy/Utils/show_flushbar.dart';
import 'package:sustain_energy/bloc/add_facility_bloc.dart';
import 'package:sustain_energy/bloc/api_service_bloc.dart';
import 'package:sustain_energy/bloc/api_service_event.dart';
import 'package:sustain_energy/bloc/api_service_state.dart';
import 'package:sustain_energy/bloc/authentication_bloc.dart';
import 'package:sustain_energy/bloc/authentication_event.dart';
import 'package:sustain_energy/bloc/bloc.dart';

class AddNewFacilityBody extends StatefulWidget {
  final UserRepository userRepository;
  AddNewFacilityBody({@required this.userRepository});

  @override
  _AddNewFacilityBodyState createState() => _AddNewFacilityBodyState();
}

enum TextFieldsType { NAME, AREA_IN_SQ_MTR, ADDRESS, POSTAL_CODE }

class TextFieldModel {
  final String label;
  final IconData iconData;
  final TextInputType type;
  final FocusNode currentFocusNode;
  final FocusNode nextFocusNode;
  bool hasError;
  String errorMessage;

  TextFieldModel(
      {@required this.iconData,
      @required this.label,
      @required this.type,
      @required this.currentFocusNode,
      @required this.nextFocusNode,
      @required this.errorMessage,
      @required this.hasError});
}

FocusNode _nameFocusNode = new FocusNode();
FocusNode _areaFocusNode = new FocusNode();
FocusNode _addressFocusNode = new FocusNode();
FocusNode _postalFocusNode = new FocusNode();
bool _autoValidateAvailable = false;

class NewFacilityModel {
  String name;
  FacilityTypeModel type;
  double area;
  String address;
  String postalCode;

  NewFacilityModel({
    @required this.name,
    @required this.address,
    @required this.area,
    @required this.type,
    @required this.postalCode,
  });
}

NewFacilityModel _facilityModel = NewFacilityModel(
  address: null,
  area: null,
  name: null,
  postalCode: null,
  type: null,
);

TextFieldModel getTextFieldWithIcon(TextFieldsType type) {
  if (type == TextFieldsType.ADDRESS) {
    return TextFieldModel(
        iconData: Icons.business,
        label: 'Facilty Address',
        type: TextInputType.text,
        currentFocusNode: _addressFocusNode,
        nextFocusNode: _postalFocusNode,
        hasError: false,
        errorMessage: '');
  } else if (type == TextFieldsType.AREA_IN_SQ_MTR) {
    return TextFieldModel(
        label: 'Facility Area in square meter',
        iconData: Icons.border_vertical,
        type: TextInputType.numberWithOptions(decimal: true),
        currentFocusNode: _areaFocusNode,
        nextFocusNode: _addressFocusNode,
        hasError: false,
        errorMessage: '');
  } else if (type == TextFieldsType.POSTAL_CODE) {
    return TextFieldModel(
        label: 'Facility Postal Code',
        iconData: Icons.local_post_office,
        type: TextInputType.numberWithOptions(decimal: true),
        currentFocusNode: _postalFocusNode,
        nextFocusNode: null,
        hasError: false,
        errorMessage: '');
  } else {
    return TextFieldModel(
        label: 'Facility Name',
        iconData: Icons.title,
        type: TextInputType.text,
        currentFocusNode: _nameFocusNode,
        nextFocusNode: _areaFocusNode,
        hasError: false,
        errorMessage: '');
  }
}

class _AddNewFacilityBodyState extends State<AddNewFacilityBody> {
  FacilityTypeModel selectedFacilty;
  FacilityTypeModel tempSelectedFacilty;
  int selectedFacilityIndex;
  int tempSelectedFacilityIndex;
  var _controller = FixedExtentScrollController(initialItem: 0);
  bool onChange;
  bool isDisabled;
  bool isLoading;
  bool isSuccess;
  var map;
  AddFacilityBloc addFacilityBloc;

  @override
  void initState() {
    onChange = false;
    isDisabled = false;
    isLoading = false;
    isSuccess = false;
    map = new Map<String, dynamic>();
    _facilityModel = NewFacilityModel(
      address: null,
      area: null,
      name: null,
      postalCode: null,
      type: null,
    );
    super.initState();
  }

  @override
  void dispose() {
    isDisabled = false;
    _controller.dispose();
    selectedFacilty = null;
    _facilityModel = null;
    _autoValidateAvailable = false;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return buildScaffold(context);
  }

  Widget buildScaffold(BuildContext context) {
    return BlocListener<AddFacilityBloc, AddFacilityState>(
        listener: (context, state) {
      if (state is DataLoadingAF) {
        isLoading = true;
        isDisabled = true;
        setState(() {});
      } else if (state is DataLoadedAF) {
        reenableFields();
        WidgetsBinding.instance.addPostFrameCallback((callback) => ShowFlushBar(
                context: context,
                title: 'Facility added successfully',
                message:
                    'Please be patient while we\'re redirecting to the home page...',
                isThisError: false,
                isSuccess: true,
                isInProgress: false,
                duration: 3)
            .show());
        Future.delayed(Duration(seconds: 3));
        Navigator.pop(context, true);
      } else if (state is DataLoadingFailedAF) {
        reenableFields();
        WidgetsBinding.instance.addPostFrameCallback((callback) => ShowFlushBar(
                context: context,
                title: 'Failed to add facility',
                message: state.error.contains('socket')
                    ? 'We\'re facing trouble to connect to the interet'
                    : state.error,
                isThisError: true,
                isSuccess: false,
                isInProgress: false,
                duration: 3)
            .show());
      }
    }, child: BlocBuilder<AddFacilityBloc, AddFacilityState>(
      builder: (context, state) {
        addFacilityBloc = BlocProvider.of<AddFacilityBloc>(context);
        return ListView(
          physics: BouncingScrollPhysics(),
          shrinkWrap: true,
          children: <Widget>[
            CustomTopContainer(
              isLoading: isLoading,
              isSuccess: isSuccess,
            ),
            SizedBox(
              height: 20,
            ),
            buildTextField(TextFieldsType.NAME),
            buildFacilityType(context),
            buildTextField(TextFieldsType.AREA_IN_SQ_MTR),
            buildTextField(TextFieldsType.ADDRESS),
            buildTextField(TextFieldsType.POSTAL_CODE),
            buildButton(isDisabled, state),
            buildContainerTempMessage()
          ],
        );
      },
    ));
  }

  Container buildContainerTempMessage() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      decoration: BoxDecoration(
          color: Colors.lightBlue.withOpacity(0.3),
          borderRadius: BorderRadius.circular(5)),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: Text(
            '*Login to your web portal to add utility data to get more acurate calculations.',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                color: Colors.black.withOpacity(0.7)),
          ),
        ),
      ),
    );
  }

  Container buildButton(bool isDisabled, AddFacilityState state) {
    print(isDisabled);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 80, vertical: 25),
      child: RaisedButton(
        padding: EdgeInsets.all(0),
        textColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80)),
        onPressed: isDisabled
            ? null
            : () async {
                if (_facilityModel.type != null &&
                    _facilityModel.area != null &&
                    _facilityModel.address != null &&
                    _facilityModel.name != null &&
                    _facilityModel.postalCode != null) {
                  prepareForSubmission(state);
                } else if (_facilityModel.area != null &&
                    _facilityModel.address != null &&
                    _facilityModel.name != null &&
                    _facilityModel.postalCode != null &&
                    _facilityModel.type == null) {
                  ShowFlushBar(
                          context: context,
                          duration: 5,
                          isInProgress: false,
                          isSuccess: false,
                          isThisError: true,
                          message: "Please select valid type.",
                          title: 'Invalid type')
                      .show();
                } else {
                  setState(() {
                    _autoValidateAvailable = true;
                  });
                }
                //_validateInputFields();
              },
        child: Container(
          padding: EdgeInsets.all(12),
          child: Center(
              child: Text(
            isDisabled ? 'Hold on...' : "Submit",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
          )),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(80),
              gradient: LinearGradient(colors: [
                isDisabled ? Colors.grey.withOpacity(0.5) : Color(0xff53d8d5),
                isDisabled ? Colors.white54 : Color(0xff52a9e9),
              ])),
        ),
      ),
    );
  }

  Padding buildTextField(TextFieldsType type) {
    var model = getTextFieldWithIcon(type);
    return Padding(
      padding: EdgeInsets.all(15),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5), topRight: Radius.circular(5)),
          color: Colors.grey[200],
        ),
        child: TextFormField(
          autovalidate: _autoValidateAvailable,
          validator: (seq) {
            return validate(model, seq);
          },
          keyboardType: model.type,
          maxLength: model.currentFocusNode == _postalFocusNode ? 6 : null,
          textInputAction: model.nextFocusNode != null
              ? TextInputAction.next
              : TextInputAction.done,
          onChanged: (char) {
            if (model.currentFocusNode == _nameFocusNode) {
              _facilityModel.name = char;
            } else if (model.currentFocusNode == _areaFocusNode) {
              _facilityModel.area = double.parse(char);
            } else if (model.currentFocusNode == _addressFocusNode) {
              _facilityModel.address = char;
            } else if (model.currentFocusNode == _postalFocusNode) {
              _facilityModel.postalCode = char;
            }
          },
          onFieldSubmitted: (arg) {
            if (model.currentFocusNode == _nameFocusNode) {
              _facilityModel.name = arg;
            } else if (model.currentFocusNode == _areaFocusNode) {
              _facilityModel.area = double.parse(arg);
            } else if (model.currentFocusNode == _addressFocusNode) {
              _facilityModel.address = arg;
            } else if (model.currentFocusNode == _postalFocusNode) {
              _facilityModel.postalCode = arg;
            }
            if (model.nextFocusNode != null) {
              _changeFocus(model.currentFocusNode, model.nextFocusNode);
            }
          },
          focusNode: model.currentFocusNode,
          cursorColor: Colors.blue,
          decoration: InputDecoration(
              contentPadding: EdgeInsets.all(10),
              focusColor: Colors.blue,
              labelText: model.label,
              labelStyle: TextStyle(
                fontWeight: FontWeight.w600,
              ),
              suffixIcon: Icon(model.iconData)),
        ),
      ),
    );
  }

  /// Prepare the other fields for submission
  void prepareForSubmission(AddFacilityState state) {
    map.clear();
    map['address'] = _facilityModel.address;
    map['area'] = _facilityModel.area.toDouble();
    map['name'] = _facilityModel.name;
    map['postal_code'] = _facilityModel.postalCode;
    map['type'] = _facilityModel.type.id;
    disableButton();
    showLoadingAtlogo(true);
    addFacilityBloc.add(AddFacility(model: map));
  }

  /// validate the fields based on the conditions.
  String validate(TextFieldModel model, String seq) {
    if (model.currentFocusNode == _nameFocusNode) {
      if (seq.isEmpty) {
        return 'Please enter the name.';
      } else if (seq.length <= 3) {
        return 'Name length should be greater than 3';
      } else {
        return null;
      }
    } else if (model.currentFocusNode == _addressFocusNode) {
      if (seq.isEmpty) {
        return 'Please enter the address.';
      } else if (seq.length <= 5) {
        return 'Address length should be greater than 5';
      } else {
        return null;
      }
    } else if (model.currentFocusNode == _areaFocusNode) {
      if (seq.isEmpty) {
        return 'Please enter the Area.';
      } else if (seq.length <= 3) {
        return 'Area length should be greater than 3';
      } else {
        return null;
      }
    } else {
      if (seq.isEmpty) {
        return 'Please enter the valid postal code.';
      } else if (seq.length <= 5) {
        return 'Area length should be greater than 4';
      } else {
        return null;
      }
    }
  }

  /// build facility type
  Padding buildFacilityType(BuildContext context) {
    print(
        'SELECTED FACILITY TYPE: ${selectedFacilty == null ? selectedFacilty.toString() : selectedFacilty.name}');
    return Padding(
      padding: EdgeInsets.all(15),
      child: InkWell(
        onTap: () {
          mShowBottomSheet(context, _controller);
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5), topRight: Radius.circular(5)),
            color: Colors.grey[200],
          ),
          child: TextFormField(
            autovalidate: _autoValidateAvailable,
            enabled: false,
            keyboardType: TextInputType.emailAddress,
            textInputAction: TextInputAction.next,
            cursorColor: Colors.blue,
            validator: (arg) {
              print(arg);
              if (arg == 'Facility Type') {
                return 'Please select valid type';
              } else {
                return null;
              }
            },
            decoration: InputDecoration(
                enabledBorder: new UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.red),
                ),
                contentPadding: EdgeInsets.all(10),
                focusColor: Colors.teal,
                labelText: selectedFacilty == null
                    ? 'Facility Type'
                    : selectedFacilty.name,
                labelStyle: TextStyle(
                  fontWeight: FontWeight.w600,
                ),
                suffixIcon: Icon(Icons.keyboard_arrow_down)),
          ),
        ),
      ),
    );
  }

  /// show bottom sheet with cupertino picker.
  void mShowBottomSheet(
      BuildContext context, FixedExtentScrollController movePointerToPosition) {
    onChange = false;

    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10))),
        context: context,
        builder: (context) {
          return BlocProvider(
            builder: (context) {
              return ApiServiceBloc(
                  userRepository: widget.userRepository, context: context)
                ..add(GetFacilityTypes());
            },
            child: BlocListener<ApiServiceBloc, ApiServiceState>(
              listener: (context, state) {
                if (state is DataLoadingFailed) {
                  Center(
                    child: Column(
                      children: <Widget>[
                        Text(state.error.contains('SocketException')
                            ? 'We\'re facing trouble to connect to the Internet'
                            : '${state.error}'),
                        SizedBox(
                          height: 10,
                        ),
                        OutlineButton(
                          onPressed: () {},
                          child: Text('Retry'),
                        )
                      ],
                    ),
                  );
                }
                if (state is DataLoading) {
                  Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
              child: BlocBuilder<ApiServiceBloc, ApiServiceState>(
                builder: (context, state) {
                  if (state is DataLoading) {
                    return Center(child: CircularProgressIndicator());
                  } else if (state is DataLoaded) {
                    var data = state.model;
                    var container = buildStaticBottomSheet(context, data);
                    return container;
                  } else if (state is DataLoadingFailed) {
                    reenableFields();
                    WidgetsBinding.instance.addPostFrameCallback((callback) =>
                        ShowFlushBar(
                                context: context,
                                duration: 3,
                                isInProgress: false,
                                isSuccess: false,
                                isThisError: true,
                                message: state.error.contains('SocketException')
                                    ? 'We\'re facing trouble to connect to the Internet'
                                    : '${state.error}',
                                title: 'Failed to Add facility..!')
                            .show());

                    return Container();
                  } else {
                    return Container();
                  }
                },
              ),
            ),
          );
        });
  }

  /// view to show an error
  Scaffold buildErrorView(DataLoadingFailed state, BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              state.error.contains('Unauthorized')
                  ? 'You\'ve been logged out, please login again.'
                  : state.error,
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: OutlineButton(
                onPressed: () {
                  state.error.contains('Unauthorized')
                      ? BlocProvider.of<AuthenticationBloc>(context)
                          .add(LoggedOut())
                      : BlocProvider.of<ApiServiceBloc>(context).add(
                          InternetConnectionRestored(
                              fromState: FromState.GET_FACILITY_TYPE));
                },
                child: Text(state.error.contains('Unauthorized')
                    ? 'Login Again'
                    : 'Retry'),
              ),
            )
          ],
        ),
      ),
    );
  }

  Container buildStaticBottomSheet(BuildContext context, data) {
    return Container(
      height: 280,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 10.0, left: 20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Select Area Serving',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      color: Colors.grey.withOpacity(0.5)),
                ),
                IconButton(
                  icon: Icon(Icons.done, color: Colors.green),
                  onPressed: () {
                    Navigator.pop(context);

                    // There are total of 3 possibilites:
                    // 1. Opened bottom sheet and closed with out selection, in this case we don't do anything.
                    // 2. Opened bottom sheet and closed with out selection, but changed the type (I mean user
                    // scrolled the list but not pressed done icon)
                    // In case 2 as user scrolled our Cupertino build picker triggers the OnSelectionChanged method which will affect the following fields:
                    // * onChange = true
                    // ** tempSelectedIndex = index;
                    // *** tempSelectedFacility = list[index].id & list[index].name
                    // 3. Opened bottom sheet and closed with selection.

                    // 620-628 code is responsible for the following points.
                    // -> It will avoid any selection that user was not intended. (like user scrolled the list but not selected.)
                    // In above case we will ignore the temp values and override them once the icon btn pressed, override with that respective data.
                    // -> It will avoid the scrolling the list to unselected item (normally our list automatically scrolls to previously selected item).

                    if (onChange) {
                      selectedFacilityIndex = tempSelectedFacilityIndex;
                      _facilityModel.type =
                          selectedFacilty = tempSelectedFacilty;
                      if (_facilityModel.type == null) {
                        _facilityModel.type = selectedFacilty =
                            new FacilityTypeModel(
                                id: data[selectedFacilityIndex].id,
                                name: data[selectedFacilityIndex].name);
                      }
                    } else {
                      _facilityModel.type = selectedFacilty =
                          new FacilityTypeModel(
                              id: data[0].id, name: data[0].name);
                    }
                    setState(() {});
                  },
                  padding: EdgeInsets.only(right: 10.0),
                  tooltip: 'Done',
                )
              ],
            ),
          ),
          BuildCupertinoPicker(
            index: selectedFacilityIndex,
            list: data,
            callback: changeType,
          )
        ],
      ),
    );
  }

  void _changeFocus(FocusNode currentFocusNode, FocusNode nextFocusNode) {
    currentFocusNode.unfocus();
    nextFocusNode.requestFocus();
    setState(() {});
  }

  void changeType(int index, List<FacilityTypeModel> list) {
    onChange = true;
    tempSelectedFacilityIndex = index;
    tempSelectedFacilty =
        new FacilityTypeModel(id: list[index].id, name: list[index].name);
  }

  void disableButton() {
    isDisabled = true;
  }

  void showLoadingAtlogo(bool param0) {
    isLoading = true;
  }

  void reenableFields() {
    isLoading = false;
    isDisabled = false;
    WidgetsBinding.instance.addPostFrameCallback((callback) => setState(() {}));
  }
}
