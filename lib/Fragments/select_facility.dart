import 'package:async_loader/async_loader.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:sustain_energy/CustomViews/custom_top.dart';
import 'package:sustain_energy/Fragments/add_equipment.dart';
import 'package:sustain_energy/Fragments/chiller_cell.dart';
import 'package:sustain_energy/Fragments/domestic_cell.dart';
import 'package:sustain_energy/Fragments/furnace_cell.dart';
import 'package:sustain_energy/Fragments/light_cell.dart';
import 'package:sustain_energy/Fragments/motor_cell.dart';
import 'package:sustain_energy/Fragments/pump_cell.dart';
import 'package:sustain_energy/Models/equipment_response.dart';
import 'package:sustain_energy/Models/equipment_type_model.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import 'package:sustain_energy/Utils/compare_list.dart';
import 'package:sustain_energy/Utils/constant.dart';
import 'package:sustain_energy/Utils/show_alert_dialog.dart';
import 'package:sustain_energy/Utils/show_flushbar.dart';
import 'package:sustain_energy/Utils/show_gradient_app_bar.dart';
import 'package:sustain_energy/bloc/bloc.dart';

import 'boiler_cell.dart';

class SelectFacility extends StatefulWidget {
  final UserRepository userRepository;
  final String facilityId;
  final String facilityLabel;
  SelectFacility(
      {@required this.userRepository,
      @required this.facilityLabel,
      @required this.facilityId});
  @override
  _SelectFacilityState createState() => _SelectFacilityState();
}

class Comparator extends Equatable {
  final String value;

  Comparator(this.value);

  @override
  List<Object> get props => [value];
}

class _SelectFacilityState extends State<SelectFacility>
    with TickerProviderStateMixin {
  AnimationController _controller;
  PageController _pageController;

  // REQUIRED ARGUMENTS FOR PAGINATION.
  int maxPage;
  int page = 1;
  int maxCount;
  EquipmentResponse response;
  ApiServiceBloc apiServiceBloc;
  List<Item> list = [];
  bool _isLoading = false;
  String _error;
  final ValueNotifier<bool> _notifier = ValueNotifier<bool>(true);
  final ValueNotifier<int> _isLoaded = ValueNotifier<int>(0);
  ScrollController _scrollController;
  // ...
  // TILL HERE FOR PAGAINATION

  static bool isOneSelected = true;
  double containerHeight = 0;
  // Unique key's for buttons add equipment & equipment already added.
  // These key's helps us to differentiate between the buttons when pressed and perform
  // unique action.
  UniqueKey key_1 = new UniqueKey();
  UniqueKey key_2 = new UniqueKey();
  // This is used to switch the position of selected tab with animation using AnimatePoistioned Widget.
  double _position = 0;

  bool _isEditable;
  static int indexExpanded;

  final GlobalKey<AnimatedListState> listKey = GlobalKey<AnimatedListState>();
  final GlobalKey<ScaffoldState> _scaffoldState =
      new GlobalKey<ScaffoldState>();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  //Temporary data which will be replaced
  static List<EquipmentTypeModel> dataList = [];

  static List imageList = [
    'https://ciciboilers.com/wp-content/uploads/2015/09/slider-boiler-01.jpg',
    'https://thumbs.dreamstime.com/b/typical-installation-pump-piping-chiller-plant-valve-tag-show-how-to-install-y-strainer-correct-direction-136850121.jpg',
    'https://www.danfoss.com/media/1579/substation.jpg?anchor=center&mode=scale&width=520',
    'https://nationalmaglab.org/media/k2/items/cache/72aaebf2db92c4cb1c4ea733fa6aab75_XL.jpg',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQiMSorMJrIV5NocQtHP0DIDGglCB8rfhMK8PdGeHd7Mos5t001',
    'https://cnet2.cbsistatic.com/img/SRAXIiVVJ9x956VP9goWCY7Q1-o=/2017/09/20/32a94ec6-03a1-4225-b799-cf2e4f3d127f/cree-floodlight-4.jpg',
    'https://www.dpccars.com/blog/wp-content/uploads/2018/07/Audi-Electric-e-tron-Motor-Factory-700x325.png',
    'http://www.testmotors.com/wp-content/uploads/2016/12/motor-hd-564x341.jpg',
  ];

  final GlobalKey<AsyncLoaderState> asyncLoadedState =
      GlobalKey<AsyncLoaderState>();

  initState() {
    super.initState();
    _scrollController = new ScrollController();
    _isEditable = false;
    _controller = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    _pageController = new PageController(initialPage: 0, keepPage: false);
  }

  @override
  dispose() {
    _scrollController.dispose();
    _controller.dispose();
    _pageController.dispose();
    super.dispose();
  }

  // Page Controller
  @override
  Widget build(BuildContext context) {
    apiServiceBloc = BlocProvider.of<ApiServiceBloc>(context);
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent ==
          _scrollController.position.pixels) {
        if (!_isLoading) {
          _isLoading = !_isLoading;
          _isLoaded.value = 0;
          //Do API call here...
          if (page + 1 <= maxPage) {
            apiServiceBloc.add(GetEquipmentResults(page: page + 1));
          } else {
            _isLoaded.value = 1;
          }
          page = page + 1;
        }
      }
    });
    return Scaffold(
      key: _scaffoldState,
      appBar: _buildGradientAppBar(),
      backgroundColor: Colors.grey[200],
      body: Column(
        children: <Widget>[CustomTopContainer(), _buildTabBar(), _buildBody()],
      ),
    );
  }

  void showSuccessMsg(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => ShowFlushBar(
            context: context,
            duration: 3,
            title: 'Data has been loaded successfully.',
            message: 'We appreciate your patience.',
            isInProgress: false,
            isThisError: false,
            isSuccess: true)
        .show());
  }

  Widget getAlreadyAddedEquipmentList(
    List<Item> data,
  ) {
    List<Item> list = data.where((item) {
      return item.facility == widget.facilityId;
    }).toList();
    return RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: _refresh,
        child: ListView(
          physics: BouncingScrollPhysics(),
          controller: _scrollController,
          shrinkWrap: true,
          children: <Widget>[
            ValueListenableBuilder(
              valueListenable: _isLoaded,
              builder: (context, val, child) {
                return (val == 0 || val == 1 && response != null)
                    ? list.isEmpty
                        ? Container(
                            height: MediaQuery.of(context).size.height / 1.5,
                            child: Center(
                              child: Text('No Data Available'),
                            ),
                          )
                        : AnimatedList(
                            key: listKey,
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            initialItemCount: list.length,
                            itemBuilder: (context, index, animation) {
                              return getTile(
                                  list, index, context, _isEditable, animation);
                            },
                          )
                    : buildErrorView(_error, context, page);
              },
            ),
            ValueListenableBuilder(
              valueListenable: _isLoaded,
              builder: (context, val, child) {
                return val == 0
                    ? Center(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: CircularProgressIndicator(),
                        ),
                      )
                    : Container();
              },
            )
          ],
        ));
  }

  Future<Null> _refresh() {
    //we will get all the equipment list again from page 1.
    apiServiceBloc.add(GetEquipmentResults(page: 1));
    return null;
  }

  Widget getTile(List<Item> data, int index, BuildContext mContext,
      bool isInEditMode, Animation animation) {
    return SizeTransition(
      axis: Axis.vertical,
      sizeFactor: animation,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          getWidget(
              data, index, context, cellTapFunctionality, getIndexExpanded),
          Material(
            color: Colors.transparent,
            child: InkWell(
                onTap: () async {
                  return isInEditMode
                      ? ShowAlertDialog(
                              title: 'Confirm',
                              message:
                                  'Are you sure you wish to delete this item?',
                              positiveButtonLabel: 'DELETE',
                              negativeButtonLabel: 'CANCEL',
                              positiveFunctionality: () {
                                positiveButtonFunctionality(
                                    mContext, data, index);
                              },
                              negativeFunctionality: null,
                              isDismissable: true,
                              type: DialogTypes.WITH_POSITIVE_NEGATIVE_BUTTON)
                          .show(context)
                      : null;
                },
                child: AnimatedContainer(
                  curve: Curves.fastLinearToSlowEaseIn,
                  duration: Duration(milliseconds: 350),
                  height: indexExpanded == index
                      ? containerHeight > 0 ? 360.0 : containerHeight
                      : containerHeight,
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.black.withOpacity(0.7)),
                  child: IconButton(
                    icon: Icon(
                      Icons.cancel,
                      color: Colors.white,
                    ),
                    iconSize: isInEditMode ? 48 : 0,
                    onPressed: null,
                  ),
                )),
          ),
        ],
      ),
    );
  }

  static int getIndexExpanded() {
    return indexExpanded;
  }

  void cellTapFunctionality(int index) {
    indexExpanded = indexExpanded == index ? null : index;
    setState(() {});
  }

  compareAndReturnType(String name) {
    return name.toLowerCase().contains('BOILER'.toLowerCase())
        ? EquipmentType.BOILER
        : name.toLowerCase().contains('CHILLER'.toLowerCase())
            ? EquipmentType.CHILLER
            : name
                    .toLowerCase()
                    .contains('Domestic Hot Water Tank'.toLowerCase())
                ? EquipmentType.DOMESTIC_HOT_WATER
                : name.toLowerCase().contains('FURNACE'.toLowerCase())
                    ? EquipmentType.FURNACE
                    : name
                            .toLowerCase()
                            .contains('OVERHEAD_HEATER'.toLowerCase())
                        ? EquipmentType.OVERHEAD_HEATER
                        : name.toLowerCase().contains('LIGHT'.toLowerCase())
                            ? EquipmentType.LIGHT
                            : name.toLowerCase().contains('MOTOR'.toLowerCase())
                                ? EquipmentType.MOTOR
                                : EquipmentType.PUMP;
  }

  Widget getWidget(List<Item> data, int index, BuildContext context,
      Function function, Function getIndex) {
    EquipmentType type = compareAndReturnType(data[index].type.name);
    return type == EquipmentType.BOILER
        ? new Boilercell(
            item: data[index],
            index: index,
            function: function,
            getIndex: getIndex,
          )
        : type == EquipmentType.CHILLER
            ? new ChillerCell(
                item: data[index],
                index: index,
                function: function,
                getIndex: getIndex,
              )
            : type == EquipmentType.DOMESTIC_HOT_WATER
                ? new DomesticCell(
                    item: data[index],
                    index: index,
                    function: function,
                    getIndex: getIndex,
                  )
                : type == EquipmentType.FURNACE
                    ? new FurnaceCell(
                        item: data[index],
                        index: index,
                        function: function,
                        getIndex: getIndex,
                      )
                    : type == EquipmentType.LIGHT
                        ? new LightCell(
                            item: data[index],
                            index: index,
                            function: function,
                            getIndex: getIndex,
                          )
                        : type == EquipmentType.MOTOR
                            ? new MotorCell(
                                item: data[index],
                                index: index,
                                function: function,
                                getIndex: getIndex,
                              )
                            : new PumpCell(
                                item: data[index],
                                index: index,
                                function: function,
                                getIndex: getIndex,
                              );
  }

  GridView getAvailableEquipementList(
      List<EquipmentTypeModel> data, BuildContext buildContext) {
    return GridView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        gridDelegate:
            new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemCount: data.length,
        itemBuilder: (context, index) {
          return Container(
            padding: EdgeInsets.all(3),
            child: Card(
              elevation: 0,
              child: Stack(
                fit: StackFit.expand,
                alignment: Alignment.center,
                children: <Widget>[
                  ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: FadeInImage(
                        placeholder:
                            AssetImage('assets/images/placeholder.png'),
                        image: NetworkImage(
                          imageList.length > index
                              ? imageList[index]
                              : 'https://developers.google.com/maps/documentation/maps-static/images/error-image-generic.png',
                        ),
                        fit: BoxFit.cover,
                      )),
                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(5),
                      splashColor: Colors.blue.withOpacity(0.5),
                      onTap: () async {
                        EquipmentType type = EquipmentType.FURNACE;
                        if (index == 1) {
                          type = EquipmentType.CHILLER;
                        } else if (index == 2) {
                          type = EquipmentType.DOMESTIC_HOT_WATER;
                        } else if (index == 3) {
                          type = EquipmentType.FURNACE;
                        } else if (index == 4) {
                          type = EquipmentType.DOMESTIC_HOT_WATER;
                        } else if (index == 5) {
                          type = EquipmentType.LIGHT;
                        } else if (index == 6) {
                          type = EquipmentType.MOTOR;
                        } else if (index == 7) {
                          type = EquipmentType.PUMP;
                        } else {
                          type = EquipmentType.BOILER;
                        }
                        bool result = await Navigator.push(context,
                            CupertinoPageRoute(builder: (context) {
                          return AddEquipment(
                            facilityLabel: widget.facilityLabel,
                            equipmentType: type,
                            userRepository: widget.userRepository,
                            equipmentTypeId: data[index].id,
                            facilityId: widget.facilityId,
                          );
                        }));
                        if (result != null && result == true) {
                          //We refreshes list again...
                          list.clear();
                          apiServiceBloc.add(GetEquipmentResults(page: 1));
                        }
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.5),
                            borderRadius: BorderRadius.circular(5)),
                        child: Center(
                          child: Text(
                            '${data[index].name}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontSize: 18),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  void configureTabBar(double position, int pageNumber, bool isSelected) {
    _position = position;
    _pageController.animateToPage(pageNumber,
        duration: Duration(milliseconds: 350), curve: Curves.linearToEaseOut);
    isOneSelected = isSelected;
    setState(() {});
  }

  positiveButtonFunctionality(
      BuildContext mContext, List<Item> data, int index) async {
    // Shows the information on Snackbar
    var response =
        await widget.userRepository.deleteEquipment(data[index].id, context);
    if (response.statusCode == 204 || response.statusCode == 200) {
      // Scaffold.of(mContext).showSnackBar(
      //     SnackBar(content: Text("${data[index].name} dismissed")));
      // Removes that item the list on swipwe
      setState(() {
        listKey.currentState.removeItem(
          index,
          (BuildContext context, Animation animation) =>
              getTile(list, index, context, false, animation),
          duration: const Duration(milliseconds: 250),
        );
      });
      Navigator.of(context).pop(true);
      list.clear();
      apiServiceBloc.add(GetEquipmentResults(page: 1));
      // Use the below code if you find any issues after deleting item,
      // The below code will ensure that there should be no errors.
      //apiServiceBloc.add(GetEquipmentResults());
    } else if (response.statusCode == 404) {
      Scaffold.of(mContext).showSnackBar(SnackBar(
          content:
              Text("No Equipment Found on Database with this equipment id")));
    } else {
      Scaffold.of(mContext).showSnackBar(SnackBar(
          content: Text(
              "We're facing trouble to delete this item, please try again later.")));
    }
  }

  // Building our own custom tab bar.
  _buildTabBar() {
    return GestureDetector(
        onPanUpdate: (details) {
          if (details.delta.dx > 0) {
            configureTabBar(MediaQuery.of(context).size.width / 2.15, 1, false);
          } else if (details.delta.dx < 0) {
            configureTabBar(0, 0, true);
          }
        },
        onTapUp: (tapDetails) {
          var localTouchPosition = (context.findRenderObject() as RenderBox)
              .globalToLocal(tapDetails.globalPosition);
          var isLeftSideTouched =
              localTouchPosition.dx < MediaQuery.of(context).size.width / 2;
          if (isLeftSideTouched) {
            // Left Side Touched
            configureTabBar(0, 0, true);
          } else {
            // Right Side Touched
            configureTabBar(MediaQuery.of(context).size.width / 2.15, 1, false);
          }
        },
        child: Container(
          height: 40,
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
          decoration: BoxDecoration(
              color: Colors.black12, borderRadius: BorderRadius.circular(5)),
          child: Stack(
            fit: StackFit.loose,
            alignment: Alignment.center,
            children: <Widget>[
              AnimatedPositioned(
                curve: Curves.fastOutSlowIn,
                duration: Duration(milliseconds: 350),
                left: _position,
                child: Container(
                  alignment: Alignment.center,
                  height: 34,
                  margin: EdgeInsets.all(5),
                  width: MediaQuery.of(context).size.width / 2.3,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Color(0xff53d8d5), Color(0xff52a9e9)]),
                      borderRadius: BorderRadius.circular(5)),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      'Add Equipment',
                      style: TextStyle(
                          color: _position == 0 ? Colors.white : Colors.grey,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  Text(
                    'Added Equipments',
                    style: TextStyle(
                        color: _position == 0 ? Colors.grey : Colors.white,
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
            ],
          ),
        ));
  }

  _buildGradientAppBar() {
    return ShowGradientAppBar(
      title: widget.facilityLabel,
      leadingIcon: OMIcons.arrowBackIos,
      leadIconClickFuncitonality: null,
      actions: <Widget>[
        Material(
          color: Colors.transparent,
          child: AnimatedContainer(
            duration: Duration(milliseconds: 500),
            child: Visibility(
              visible: !isOneSelected,
              child: AnimatedContainer(
                duration: Duration(milliseconds: 350),
                curve: Curves.easeOut,
                child: IconButton(
                  icon: Icon(
                    _isEditable ? Icons.close : Icons.edit,
                    color: Colors.white,
                  ),
                  padding: EdgeInsets.all(5),
                  tooltip: 'Edit Equipments',
                  onPressed: () async {
                    _isEditable = !_isEditable;
                    setState(() {
                      _isEditable
                          ? containerHeight = 170.0
                          : containerHeight = 0.0;
                    });
                  },
                ),
              ),
            ),
          ),
        )
      ],
    ).show(context);
  }

  _buildBody() {
    return Expanded(
      flex: 2,
      child: PageView(
        controller: _pageController,
        scrollDirection: Axis.horizontal,
        onPageChanged: (index) {
          if (index == 0) {
            configureTabBar(0, 0, true);
          } else {
            configureTabBar(MediaQuery.of(context).size.width / 2.15, 1, false);
          }
        },
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          _buildPageOneWithBloc(),
          _buildBodyWithBloc(),
        ],
      ),
    );
  }

  _buildPageOneWithBloc() {
    return BlocListener<GetequipmenttypeBloc, GetequipmenttypeState>(
      listener: (context, state) {
        if (state is DataLoadingFailedGE) {
          WidgetsBinding.instance.addPostFrameCallback((callback) => ShowFlushBar(
                  duration: 3,
                  context: context,
                  title: 'Data Loading Failure',
                  message: state.error.contains('SocketException')
                      ? 'You\'re not connected to Internet, please try again after connection established'
                      : '${state.error}',
                  isInProgress: false,
                  isThisError: true,
                  isSuccess: false)
              .show());
        }
        if (state is DataLoadingGE) {
          buildLoadingWidget(context);
        }
        if (state is DataLoadedGE) {
          showSuccessMsg(context);
        }
      },
      child: BlocBuilder<GetequipmenttypeBloc, GetequipmenttypeState>(
        builder: (context, state) {
          if (state is DataLoadingGE) {
            return buildLoadingWidget(context);
          } else if (state is DataLoadedGE) {
            dataList.clear();
            dataList.addAll(state.model);
            return getAvailableEquipementList(dataList, context);
          } else if (state is DataLoadingFailedGE) {
            ShowFlushBar(
                    duration: 3,
                    context: context,
                    title: 'Data Loading Failure',
                    message: state.error.contains('SocketException')
                        ? 'You\'re not connected to Internet, please try again after connection established'
                        : '${state.error}',
                    isInProgress: false,
                    isThisError: true,
                    isSuccess: false)
                .show();
            return buildErrorView(state.error, context, 0);
          } else {
            return Container();
          }
        },
      ),
    );
  }

  _buildBodyWithBloc() {
    return BlocListener<ApiServiceBloc, ApiServiceState>(
        listener: (context, state) {
      if (state is DataLoadingFailed) {
        _notifier.value = false;
        _isLoaded.value = 2;
        _error = state.error.toString();
        buildErrorBar(context, state);
      }
      if (state is DataLoading) {
        _notifier.value = true;
        _isLoaded.value = 0;
        buildLoadingWidget(context);
      }
      if (state is DataLoaded) {
        showSuccessMsg(context);
        _notifier.value = false;
        _isLoaded.value = 1;
        response = state.model is EquipmentResponse ? response : null;
      }
    }, child: BlocBuilder<ApiServiceBloc, ApiServiceState>(
      builder: (context, state) {
        if (state is DataLoading) {
          _notifier.value = true;
          _isLoaded.value = 0;
          return buildLoadingWidget(context);
        } else if (state is DataLoaded) {
          return dataLoadedInitBuild(state);
        } else if (state is DataLoadingFailed) {
          _notifier.value = false;
          _error = state.error.contains('Unauthorized')
              ? 'You\'ve been logged out, please login again.'
              : state.error.contains('SocketException')
                  ? 'We\'re facing trouble to connect to the Internet'
                  : state.error.toString();
          buildErrorBar(context, state);
          _isLoaded.value = 2;
          return buildErrorView(state.error, context, 1);
        } else {
          return Container();
        }
      },
    ));
  }

  Widget dataLoadedInitBuild(DataLoaded state) {
    _notifier.value = false;
    _isLoaded.value = 1;
    response = state.model is EquipmentResponse ? state.model : null;
    maxPage = response.pages;
    maxCount = response.count;
    _isLoading = false;
    if (list.isEmpty) {
      list.addAll(response.items);
    } else {
      List<Item> filteredList =
          ComplexListCompare.help<Item>(list, response.items);
      if (filteredList.isNotEmpty) {
        var newList = new List<Item>();
        newList.addAll(filteredList);
        list.clear();
        list.addAll(newList);
      }
      response.items.clear();
      response.items.addAll(list);
    }
    if (ShowFlushBar.flushbar != null) {
      ShowFlushBar.flushbar.dismiss();
    }
    return getAlreadyAddedEquipmentList(response.items);
  }

  void buildErrorBar(BuildContext context, DataLoadingFailed state) {
    ShowFlushBar(
            duration: 3,
            context: context,
            title: 'Data Loading Failure',
            message: state.error.contains('SocketException')
                ? 'We\'re facing trouble to connect to the Internet'
                : '${state.error}',
            isInProgress: false,
            isThisError: true,
            isSuccess: false)
        .show();
  }

  Center buildLoadingWidget(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => ShowFlushBar(
            context: context,
            title: 'Data is Loading',
            message: 'Please be patient...',
            duration: null,
            isInProgress: true,
            isThisError: false,
            isSuccess: false)
        .show());
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Center buildErrorView(String state, BuildContext context, int page) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            state.contains('Unauthorized')
                ? 'You\'ve been logged out, please login again.'
                : state.contains('SocketException')
                    ? 'We\'re facing trouble to connect to the Internet'
                    : state.toString(),
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: OutlineButton(
              onPressed: () {
                state.contains('Unauthorized')
                    ? BlocProvider.of<AuthenticationBloc>(context)
                        .add(LoggedOut())
                    : page == 0
                        ? BlocProvider.of<GetequipmenttypeBloc>(context).add(
                            InternetConnectionRestoredGE(
                                fromState: FromState.GET_EQUIPMENT_TYPE))
                        : apiServiceBloc.add(InternetConnectionRestored(
                            fromState: FromState.GET_EQUIPMENT_RESULTS));
              },
              child: Text(
                  state.contains('Unauthorized') ? 'Login Again' : 'Retry'),
            ),
          )
        ],
      ),
    );
  }
}
