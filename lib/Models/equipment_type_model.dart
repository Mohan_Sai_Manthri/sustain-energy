import 'get_euipment_type.dart';

class EquipmentTypeModel {
  final String id;
  final String name;
  OptionalProperties optionalProperties;
  RequiredProperties requiredProperties;
  EquipmentTypeModel(
      {this.id, this.name, this.requiredProperties, this.optionalProperties});
}
