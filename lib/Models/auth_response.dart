
class AuthReponseBody {
  String authToken;
  String next;

  AuthReponseBody({this.authToken, this.next});
  factory AuthReponseBody.fromJson(Map<String, dynamic> json) =>
      AuthReponseBody(authToken: json['auth_token'], next: json['next']);

  Map<String, dynamic> toJson() => {'auth_token': authToken, 'next': next};
}

