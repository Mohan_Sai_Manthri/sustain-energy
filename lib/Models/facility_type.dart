class FacilityTypeModel {
  final String id;
  final String name;

  FacilityTypeModel({this.id, this.name});
}
