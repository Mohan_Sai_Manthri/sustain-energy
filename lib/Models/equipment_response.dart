// To parse this JSON data, do
//
//     final equipmentResponse = equipmentResponseFromJson(jsonString);

import 'dart:convert';

EquipmentResponse equipmentResponseFromJson(String str) =>
    EquipmentResponse.fromJson(json.decode(str));

String equipmentResponseToJson(EquipmentResponse data) =>
    json.encode(data.toJson());

class EquipmentResponse {
  final int count;
  final bool hasNext;
  final bool hasPrev;
  final List<Item> items;
  final int pages;

  EquipmentResponse({
    this.count,
    this.hasNext,
    this.hasPrev,
    this.items,
    this.pages,
  });

  factory EquipmentResponse.fromJson(Map<String, dynamic> json) =>
      EquipmentResponse(
        count: json["count"] == null ? null : json["count"],
        hasNext: json["has_next"] == null ? null : json["has_next"],
        hasPrev: json["has_prev"] == null ? null : json["has_prev"],
        items: json["items"] == null
            ? null
            : List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
        pages: json["pages"] == null ? null : json["pages"],
      );

  Map<String, dynamic> toJson() => {
        "count": count == null ? null : count,
        "has_next": hasNext == null ? null : hasNext,
        "has_prev": hasPrev == null ? null : hasPrev,
        "items": items == null
            ? null
            : List<dynamic>.from(items.map((x) => x.toJson())),
        "pages": pages == null ? null : pages,
      };
}

class Item {
  final DateTime createdAt;
  final String facility;
  final String id;
  final String name;
  final Nameplate nameplate;
  final List<Nameplate> photos;
  final Properties properties;
  final Type type;

  Item({
    this.createdAt,
    this.facility,
    this.id,
    this.name,
    this.nameplate,
    this.photos,
    this.properties,
    this.type,
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        facility: json["facility"] == null ? null : json["facility"],
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        nameplate: json["nameplate"] == null
            ? null
            : Nameplate.fromJson(json["nameplate"]),
        photos: json["photos"] == null
            ? null
            : List<Nameplate>.from(
                json["photos"].map((x) => Nameplate.fromJson(x))),
        properties: json["properties"] == null
            ? null
            : Properties.fromJson(json["properties"]),
        type: json["type"] == null ? null : Type.fromJson(json["type"]),
      );

  Map<String, dynamic> toJson() => {
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "facility": facility == null ? null : facility,
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "nameplate": nameplate == null ? null : nameplate.toJson(),
        "photos": photos == null
            ? null
            : List<dynamic>.from(photos.map((x) => x.toJson())),
        "properties": properties == null ? null : properties.toJson(),
        "type": type == null ? null : type.toJson(),
      };
}

class Nameplate {
  final String filename;
  final String url;

  Nameplate({
    this.filename,
    this.url,
  });

  factory Nameplate.fromJson(Map<String, dynamic> json) => Nameplate(
        filename: json["filename"] == null ? null : json["filename"],
        url: json["url"] == null ? null : json["url"],
      );

  Map<String, dynamic> toJson() => {
        "filename": filename == null ? null : filename,
        "url": url == null ? null : url,
      };
}

class Properties {
  final double areaServing;
  final double efficiencyPercentage;
  final double inputBtuH;
  final String manufacturer;
  final double outputBtuH;
  final String serialNumber;
  final bool hasVariableFrequencyDrive;
  final double powerHp;
  final int rpm;
  final int lightsCount;
  final String type;
  final int capacityLitres;
  final double recoveryRateGpm;

  Properties({
    this.areaServing,
    this.efficiencyPercentage,
    this.inputBtuH,
    this.manufacturer,
    this.outputBtuH,
    this.serialNumber,
    this.hasVariableFrequencyDrive,
    this.powerHp,
    this.rpm,
    this.lightsCount,
    this.type,
    this.capacityLitres,
    this.recoveryRateGpm,
  });

  factory Properties.fromJson(Map<String, dynamic> json) => Properties(
        areaServing: json["area_serving"] == null
            ? null
            : json["area_serving"].toDouble(),
        efficiencyPercentage: json["efficiency_percentage"] == null
            ? null
            : json["efficiency_percentage"].toDouble(),
        inputBtuH:
            json["input_btu_h"] == null ? null : json["input_btu_h"].toDouble(),
        manufacturer: json["manufacturer"] == null ? null : json["manufacturer"],
        outputBtuH: json["output_btu_h"] == null
            ? null
            : json["output_btu_h"].toDouble(),
        serialNumber:
            json["serial_number"] == null ? null : json["serial_number"],
        hasVariableFrequencyDrive: json["has_variable_frequency_drive"] == null
            ? null
            : json["has_variable_frequency_drive"],
        powerHp: json["power_hp"] == null ? null : json["power_hp"],
        rpm: json["rpm"] == null ? null : json["rpm"],
        lightsCount: json["lights_count"] == null ? null : json["lights_count"],
        type: json["type"] == null ? null : json["type"],
        capacityLitres:
            json["capacity_litres"] == null ? null : json["capacity_litres"],
        recoveryRateGpm: json["recovery_rate_gpm"] == null
            ? null
            : json["recovery_rate_gpm"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "area_serving": areaServing == null ? null : areaServing,
        "efficiency_percentage":
            efficiencyPercentage == null ? null : efficiencyPercentage,
        "input_btu_h": inputBtuH == null ? null : inputBtuH,
        "manufacturer": manufacturer == null ? null : manufacturer,
        "output_btu_h": outputBtuH == null ? null : outputBtuH,
        "serial_number": serialNumber == null ? null : serialNumber,
        "has_variable_frequency_drive": hasVariableFrequencyDrive == null
            ? null
            : hasVariableFrequencyDrive,
        "power_hp": powerHp == null ? null : powerHp,
        "rpm": rpm == null ? null : rpm,
        "lights_count": lightsCount == null ? null : lightsCount,
        "type": type == null ? null : type,
        "capacity_litres": capacityLitres == null ? null : capacityLitres,
        "recovery_rate_gpm": recoveryRateGpm == null ? null : recoveryRateGpm,
      };
}

class Type {
  final String id;
  final String name;

  Type({
    this.id,
    this.name,
  });

  factory Type.fromJson(Map<String, dynamic> json) => Type(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
      };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
