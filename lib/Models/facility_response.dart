import 'dart:convert';

FacilityResponse facilityResponseFromJson(String str) =>
    FacilityResponse.fromJson(json.decode(str));

String facilityResponseToJson(FacilityResponse data) =>
    json.encode(data.toJson());

class FacilityResponse {
  int count;
  bool hasNext;
  bool hasPrev;
  List<Item> items;
  int pages;

  FacilityResponse({
    this.count,
    this.hasNext,
    this.hasPrev,
    this.items,
    this.pages,
  });

  factory FacilityResponse.fromJson(Map<String, dynamic> json) =>
      FacilityResponse(
        count: json["count"],
        hasNext: json["has_next"],
        hasPrev: json["has_prev"],
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
        pages: json["pages"],
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "has_next": hasNext,
        "has_prev": hasPrev,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
        "pages": pages,
      };
}

class Item {
  String address;
  double area;
  Benchmark benchmark;
  DateTime createdAt;
  String id;
  String name;
  String postalCode;
  Type type;
  String user;

  Item({
    this.address,
    this.area,
    this.benchmark,
    this.createdAt,
    this.id,
    this.name,
    this.postalCode,
    this.type,
    this.user,
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        address: json["address"],
        area: json["area"],
        benchmark: Benchmark.fromJson(json["benchmark"]),
        createdAt: DateTime.parse(json["created_at"]),
        id: json["id"],
        name: json["name"],
        postalCode: json["postal_code"],
        type: Type.fromJson(json["type"]),
        user: json["user"],
      );

  Map<String, dynamic> toJson() => {
        "address": address,
        "area": area,
        "benchmark": benchmark.toJson(),
        "created_at": createdAt.toIso8601String(),
        "id": id,
        "name": name,
        "postal_code": postalCode,
        "type": type.toJson(),
        "user": user,
      };
}

class Benchmark {
  double benchmarkCostPerSquareMeter;
  double benchmarkEui;
  double costPerGj;
  double costPerSquareMeter;
  double eui;
  double totalCost;
  double totalUsage;

  Benchmark({
    this.benchmarkCostPerSquareMeter,
    this.benchmarkEui,
    this.costPerGj,
    this.costPerSquareMeter,
    this.eui,
    this.totalCost,
    this.totalUsage,
  });

  factory Benchmark.fromJson(Map<String, dynamic> json) => Benchmark(
        benchmarkCostPerSquareMeter:
            json["benchmark_cost_per_square_meter"] == null
                ? null
                : json["benchmark_cost_per_square_meter"].toDouble(),
        benchmarkEui: json["benchmark_eui"] == null
            ? null
            : json["benchmark_eui"].toDouble(),
        costPerGj:
            json["cost_per_gj"] == null ? null : json["cost_per_gj"].toDouble(),
        costPerSquareMeter: json["cost_per_square_meter"] == null
            ? null
            : json["cost_per_square_meter"].toDouble(),
        eui: json["eui"] == null ? null : json["eui"].toDouble(),
        totalCost:
            json["total_cost"] == null ? null : json["total_cost"].toDouble(),
        totalUsage:
            json["total_usage"] == null ? null : json["total_usage"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "benchmark_cost_per_square_meter": benchmarkCostPerSquareMeter == null
            ? null
            : benchmarkCostPerSquareMeter,
        "benchmark_eui": benchmarkEui,
        "cost_per_gj": costPerGj == null ? null : costPerGj,
        "cost_per_square_meter":
            costPerSquareMeter == null ? null : costPerSquareMeter,
        "eui": eui,
        "total_cost": totalCost,
        "total_usage": totalUsage,
      };
}

class Type {
  String id;
  String name;

  Type({
    this.id,
    this.name,
  });

  factory Type.fromJson(Map<String, dynamic> json) => Type(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}
