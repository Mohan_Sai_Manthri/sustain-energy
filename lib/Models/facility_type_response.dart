
import 'dart:convert';

FacilityType facilityTypeFromJson(String str) => FacilityType.fromJson(json.decode(str));

String facilityTypeToJson(FacilityType data) => json.encode(data.toJson());

class FacilityType {
    int count;
    bool hasNext;
    bool hasPrev;
    List<Item> items;
    int pages;

    FacilityType({
        this.count,
        this.hasNext,
        this.hasPrev,
        this.items,
        this.pages,
    });

    factory FacilityType.fromJson(Map<String, dynamic> json) => FacilityType(
        count: json["count"],
        hasNext: json["has_next"],
        hasPrev: json["has_prev"],
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
        pages: json["pages"],
    );

    Map<String, dynamic> toJson() => {
        "count": count,
        "has_next": hasNext,
        "has_prev": hasPrev,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
        "pages": pages,
    };
}

class Item {
    String id;
    String name;

    Item({
        this.id,
        this.name,
    });

    factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["id"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
    };
}