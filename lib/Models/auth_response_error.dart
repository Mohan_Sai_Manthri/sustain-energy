import 'dart:convert';

ErrorFromResponse errorFromJson(String str) => ErrorFromResponse.fromJson(json.decode(str));

String errorToJson(ErrorFromResponse data) => json.encode(data.toJson());

class ErrorFromResponse {
    Errors errors;

    ErrorFromResponse({
        this.errors,
    });

    factory ErrorFromResponse.fromJson(Map<String, dynamic> json) => ErrorFromResponse(
        errors: Errors.fromJson(json["errors"]),
    );

    Map<String, dynamic> toJson() => {
        "errors": errors.toJson(),
    };
}

class Errors {
    String password;
    String email;

    Errors({
        this.password,this.email
    });

    factory Errors.fromJson(Map<String, dynamic> json) => Errors(
        password: json["password"]??'',
        email: json['email']??''
    );

    Map<String, dynamic> toJson() => {
        "password": password,
    };
}