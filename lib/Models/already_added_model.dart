import 'package:flutter/material.dart';
import 'package:sustain_energy/Utils/constant.dart';

class AaeModel {
  String title;
  String input;
  String output;
  String efficiency;
  String serailNumber;
  String areaServing;
  String manufacturer;
  List<String> photoUrl;
  String facilityId;
  String id;
  String lightsCount;
  String horsePower;
  String capacityInLtr;
  String recoveryGateGPM;
  String rpm;
  EquipmentType type;
  bool variableFreqEquipped;
  String lightType;
  AaeModel(
      {@required this.title,
      @required this.lightType,
      @required this.input,
      @required this.output,
      @required this.efficiency,
      @required this.serailNumber,
      @required this.areaServing,
      @required this.manufacturer,
      @required this.photoUrl,
      @required this.facilityId,
      @required this.id,
      @required this.recoveryGateGPM,
      @required this.capacityInLtr,
      @required this.horsePower,
      @required this.lightsCount,
      @required this.rpm,
      @required this.variableFreqEquipped,
      @required this.type});
}
