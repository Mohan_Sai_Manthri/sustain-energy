import 'dart:convert';

AddFacilityResponse addFacilityResponseFromJson(String str) =>
    AddFacilityResponse.fromJson(json.decode(str));

String addFacilityResponseToJson(AddFacilityResponse data) =>
    json.encode(data.toJson());

class AddFacilityResponse {
  String address;
  double area;
  String createdAt;
  String id;
  String name;
  String postalCode;
  String type;
  String user;

  AddFacilityResponse({
    this.address,
    this.area,
    this.createdAt,
    this.id,
    this.name,
    this.postalCode,
    this.type,
    this.user,
  });

  factory AddFacilityResponse.fromJson(Map<String, dynamic> json) =>
      AddFacilityResponse(
        address: json["address"],
        area: json["area"],
        createdAt: json["created_at"],
        id: json["id"],
        name: json["name"],
        postalCode: json["postal_code"],
        type: json["type"],
        user: json["user"],
      );

  Map<String, dynamic> toJson() => {
        "address": address,
        "area": area,
        "created_at": createdAt,
        "id": id,
        "name": name,
        "postal_code": postalCode,
        "type": type,
        "user": user,
      };
}
