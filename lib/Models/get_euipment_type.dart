
import 'dart:convert';

GetEquipmentTypes getEquipmentTypesFromJson(String str) => GetEquipmentTypes.fromJson(json.decode(str));

String getEquipmentTypesToJson(GetEquipmentTypes data) => json.encode(data.toJson());

class GetEquipmentTypes {
    int count;
    bool hasNext;
    bool hasPrev;
    List<Item> items;
    int pages;

    GetEquipmentTypes({
        this.count,
        this.hasNext,
        this.hasPrev,
        this.items,
        this.pages,
    });

    factory GetEquipmentTypes.fromJson(Map<String, dynamic> json) => GetEquipmentTypes(
        count: json["count"],
        hasNext: json["has_next"],
        hasPrev: json["has_prev"],
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
        pages: json["pages"],
    );

    Map<String, dynamic> toJson() => {
        "count": count,
        "has_next": hasNext,
        "has_prev": hasPrev,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
        "pages": pages,
    };
}

class Item {
    String id;
    String name;
    OptionalProperties optionalProperties;
    RequiredProperties requiredProperties;

    Item({
        this.id,
        this.name,
        this.optionalProperties,
        this.requiredProperties,
    });

    factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["id"],
        name: json["name"],
        optionalProperties: OptionalProperties.fromJson(json["optional_properties"]),
        requiredProperties: RequiredProperties.fromJson(json["required_properties"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "optional_properties": optionalProperties.toJson(),
        "required_properties": requiredProperties.toJson(),
    };
}

class OptionalProperties {
    OptionalProperties();

    factory OptionalProperties.fromJson(Map<String, dynamic> json) => OptionalProperties(
    );

    Map<String, dynamic> toJson() => {
    };
}

class RequiredProperties {
    String areaServing;
    String efficiencyPercentage;
    String inputBtuH;
    String manufacturer;
    String outputBtuH;
    String serialNumber;
    String capacityLitres;
    String recoveryRateGpm;
    String lightsCount;
    String type;
    String powerHp;
    String rpm;
    String hasVariableFrequencyDrive;

    RequiredProperties({
        this.areaServing,
        this.efficiencyPercentage,
        this.inputBtuH,
        this.manufacturer,
        this.outputBtuH,
        this.serialNumber,
        this.capacityLitres,
        this.recoveryRateGpm,
        this.lightsCount,
        this.type,
        this.powerHp,
        this.rpm,
        this.hasVariableFrequencyDrive,
    });

    factory RequiredProperties.fromJson(Map<String, dynamic> json) => RequiredProperties(
        areaServing: json["area_serving"] == null ? null : json["area_serving"],
        efficiencyPercentage: json["efficiency_percentage"] == null ? null : json["efficiency_percentage"],
        inputBtuH: json["input_btu_h"] == null ? null : json["input_btu_h"],
        manufacturer: json["manufacturer"] == null ? null : json["manufacturer"],
        outputBtuH: json["output_btu_h"] == null ? null : json["output_btu_h"],
        serialNumber: json["serial_number"] == null ? null : json["serial_number"],
        capacityLitres: json["capacity_litres"] == null ? null : json["capacity_litres"],
        recoveryRateGpm: json["recovery_rate_gpm"] == null ? null : json["recovery_rate_gpm"],
        lightsCount: json["lights_count"] == null ? null : json["lights_count"],
        type: json["type"] == null ? null : json["type"],
        powerHp: json["power_hp"] == null ? null : json["power_hp"],
        rpm: json["rpm"] == null ? null : json["rpm"],
        hasVariableFrequencyDrive: json["has_variable_frequency_drive"] == null ? null : json["has_variable_frequency_drive"],
    );

    Map<String, dynamic> toJson() => {
        "area_serving": areaServing == null ? null : areaServing,
        "efficiency_percentage": efficiencyPercentage == null ? null : efficiencyPercentage,
        "input_btu_h": inputBtuH == null ? null : inputBtuH,
        "manufacturer": manufacturer == null ? null : manufacturer,
        "output_btu_h": outputBtuH == null ? null : outputBtuH,
        "serial_number": serialNumber == null ? null : serialNumber,
        "capacity_litres": capacityLitres == null ? null : capacityLitres,
        "recovery_rate_gpm": recoveryRateGpm == null ? null : recoveryRateGpm,
        "lights_count": lightsCount == null ? null : lightsCount,
        "type": type == null ? null : type,
        "power_hp": powerHp == null ? null : powerHp,
        "rpm": rpm == null ? null : rpm,
        "has_variable_frequency_drive": hasVariableFrequencyDrive == null ? null : hasVariableFrequencyDrive,
    };
}