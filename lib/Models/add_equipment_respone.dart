// To parse this JSON data, do
//
//     final addEquipmentResponse = addEquipmentResponseFromJson(jsonString);

import 'dart:convert';

AddEquipmentResponse addEquipmentResponseFromJson(String str) => AddEquipmentResponse.fromJson(json.decode(str));

String addEquipmentResponseToJson(AddEquipmentResponse data) => json.encode(data.toJson());

class AddEquipmentResponse {
    DateTime createdAt;
    String facility;
    String id;
    String name;
    dynamic nameplate;
    List<dynamic> photos;
    Properties properties;
    Type type;

    AddEquipmentResponse({
        this.createdAt,
        this.facility,
        this.id,
        this.name,
        this.nameplate,
        this.photos,
        this.properties,
        this.type,
    });

    factory AddEquipmentResponse.fromJson(Map<String, dynamic> json) => AddEquipmentResponse(
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        facility: json["facility"] == null ? null : json["facility"],
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        nameplate: json["nameplate"],
        photos: json["photos"] == null ? null : List<dynamic>.from(json["photos"].map((x) => x)),
        properties: json["properties"] == null ? null : Properties.fromJson(json["properties"]),
        type: json["type"] == null ? null : Type.fromJson(json["type"]),
    );

    Map<String, dynamic> toJson() => {
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "facility": facility == null ? null : facility,
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "nameplate": nameplate,
        "photos": photos == null ? null : List<dynamic>.from(photos.map((x) => x)),
        "properties": properties == null ? null : properties.toJson(),
        "type": type == null ? null : type.toJson(),
    };
}

class Properties {
    double areaServing;
    double efficiencyPercentage;
    double inputBtuH;
    String manufacturer;
    double outputBtuH;
    int lightsCount;
    String type;
    double powerHp;
    int rpm;
    int capacityLitres;
    bool hasVariableFrequencyDrive;
    double recoveryRateGpm;
    String serialNumber;

    Properties({
        this.areaServing,
        this.efficiencyPercentage,
        this.inputBtuH,
        this.manufacturer,
        this.outputBtuH,
        this.lightsCount,
        this.type,
        this.powerHp,
        this.rpm,
        this.capacityLitres,
        this.hasVariableFrequencyDrive,
        this.recoveryRateGpm,
        this.serialNumber,
    });

    factory Properties.fromJson(Map<String, dynamic> json) => Properties(
        areaServing: json["area_serving"] == null ? null : json["area_serving"],
        efficiencyPercentage: json["efficiency_percentage"] == null ? null : json["efficiency_percentage"].toDouble(),
        inputBtuH: json["input_btu_h"] == null ? null : json["input_btu_h"].toDouble(),
        manufacturer: json["manufacturer"] == null ? null : json["manufacturer"],
        outputBtuH: json["output_btu_h"] == null ? null : json["output_btu_h"].toDouble(),
        lightsCount: json["lights_count"] == null ? null : json["lights_count"],
        type: json["type"] == null ? null : json["type"],
        powerHp: json["power_hp"] == null ? null : json["power_hp"].toDouble(),
        rpm: json["rpm"] == null ? null : json["rpm"],
        capacityLitres: json["capacity_litres"] == null ? null : json["capacity_litres"],
        hasVariableFrequencyDrive: json["has_variable_frequency_drive"] == null ? null : json["has_variable_frequency_drive"],
        recoveryRateGpm: json["recovery_rate_gpm"] == null ? null : json["recovery_rate_gpm"].toDouble(),
        serialNumber: json["serial_number"] == null ? null : json["serial_number"],
    );

    Map<String, dynamic> toJson() => {
        "area_serving": areaServing == null ? null : areaServing,
        "efficiency_percentage": efficiencyPercentage == null ? null : efficiencyPercentage,
        "input_btu_h": inputBtuH == null ? null : inputBtuH,
        "manufacturer": manufacturer == null ? null : manufacturer,
        "output_btu_h": outputBtuH == null ? null : outputBtuH,
        "lights_count": lightsCount == null ? null : lightsCount,
        "type": type == null ? null : type,
        "power_hp": powerHp == null ? null : powerHp,
        "rpm": rpm == null ? null : rpm,
        "capacity_litres": capacityLitres == null ? null : capacityLitres,
        "has_variable_frequency_drive": hasVariableFrequencyDrive == null ? null : hasVariableFrequencyDrive,
        "recovery_rate_gpm": recoveryRateGpm == null ? null : recoveryRateGpm,
        "serial_number": serialNumber == null ? null : serialNumber,
    };
}

class Type {
    String id;
    String name;

    Type({
        this.id,
        this.name,
    });

    factory Type.fromJson(Map<String, dynamic> json) => Type(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
    };
}
