import 'dart:convert';

AddFacilityError addFacilityErrorFromJson(String str) =>
    AddFacilityError.fromJson(json.decode(str));

String addFacilityErrorToJson(AddFacilityError data) =>
    json.encode(data.toJson());

class AddFacilityError {
  String type;
  String title;
  int status;
  String detail;
  String instance;

  AddFacilityError({
    this.type,
    this.title,
    this.status,
    this.detail,
    this.instance,
  });

  factory AddFacilityError.fromJson(Map<String, dynamic> json) =>
      AddFacilityError(
        type: json["type"],
        title: json["title"],
        status: json["status"],
        detail: json["detail"],
        instance: json["instance"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "title": title,
        "status": status,
        "detail": detail,
        "instance": instance,
      };
}
