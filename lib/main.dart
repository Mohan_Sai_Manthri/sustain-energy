import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:sustain_energy/Fragments/login.dart';
import 'package:sustain_energy/Fragments/splash.dart';
import 'package:sustain_energy/Network/api_service.dart';
import 'package:bloc/bloc.dart';
import 'package:sustain_energy/Repository/user_repository.dart';
import 'package:sustain_energy/Utils/loading_indicator.dart';
import 'package:sustain_energy/bloc/bloc.dart';
import 'Fragments/select_main_facility.dart';

void main() {
  BlocSupervisor.delegate = SimpleAppDelegate();
  final UserRepository userRepository = UserRepository();
  runApp(Provider(
      builder: (context) => ApiService.create(),
      dispose: (context, Dio service) => service.close(),
      child: MultiBlocProvider(
        child: MyApp(
          userRepository: userRepository,
        ),
        providers: [
          BlocProvider<AuthenticationBloc>(
            builder: (context) {
              return AuthenticationBloc(
                  userRepository: userRepository, context: context)
                ..add(AppStarted());
            },
          ),
          BlocProvider<UploadingBloc>(
            builder: (context) {
              return UploadingBloc();
            },
          ),
          BlocProvider<ApiServiceBloc>(
            builder: (context) {
              return ApiServiceBloc(
                  userRepository: userRepository, context: context);
            },
          ),
          BlocProvider<FacilityBloc>(
            builder: (context) {
              return FacilityBloc(
                  userRepository: userRepository, context: context);
            },
          ),
          BlocProvider<GetequipmenttypeBloc>(
            builder: (context) {
              return GetequipmenttypeBloc(
                  userRepository: userRepository, context: context);
            },
          ),
          BlocProvider<ApiServiceBloc>(
            builder: (context) {
              return ApiServiceBloc(
                  userRepository: userRepository, context: context);
            },
          )
        ],
      ),));
}

class SimpleAppDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print(error);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }
}

class MyApp extends StatelessWidget {
  final UserRepository userRepository;
  MyApp({Key key, @required this.userRepository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
        title: 'Sustain Energy',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          builder: (context, state) {
            if (state is Uninitialized) {
              return Splash(
                userRepository: userRepository,
              );
            } else if (state is Authenticated) {
              return SelectMainFacility(
                userRepository: userRepository,
                mContext: context,
              );
            } else if (state is UnAuthenticated) {
              return LoginPage(
                userRepository: userRepository,
                mContext: context,
              );
            } else if (state is Loading) {
              return LoadingIndicator();
            } else {
              return Container(
                child: Center(
                  child: Text('Error : Unspecified State'),
                ),
              );
            }
          },
        ));
  }
}
