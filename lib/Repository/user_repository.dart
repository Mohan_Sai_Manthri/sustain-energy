import 'dart:io';

import 'package:dio/dio.dart' as prefix1;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sustain_energy/Models/add_equipment_respone.dart';
import 'package:sustain_energy/Models/add_facility_response.dart';
import 'package:sustain_energy/Models/equipment_response.dart';
import 'package:sustain_energy/Models/equipment_response.dart' as prefix2;
import 'package:sustain_energy/Models/equipment_type_model.dart';
import 'package:sustain_energy/Models/facility_response.dart';
import 'package:sustain_energy/Models/facility_response.dart' as prefix0;
import 'package:sustain_energy/Models/facility_type.dart';
import 'package:sustain_energy/Models/facility_type_response.dart';
import 'package:sustain_energy/Models/get_euipment_type.dart';
import 'package:sustain_energy/Network/api_service.dart';
import 'package:sustain_energy/Utils/constants.dart';
import 'package:sustain_energy/Utils/secure_storage.dart';
import 'package:sustain_energy/Utils/show_flushbar.dart';
import 'package:sustain_energy/database/moor_database.dart';

class UserRepository {
  List<prefix0.Item> faclityList = [];
  final secureInstance = SecureStorage.getSecureInstance();

  Future<String> authenticate(
      {@required String username,
      @required String password,
      @required BuildContext context}) async {
    // [TIP] Use this to make online api request the below fun also stores the token for later api requests!
       final response = await Provider.of<ApiService>(context)
        .loginUser({'email': username, 'password': password});
/*    String response = '';
    if (username == "user1@sustainergy.com" && password == "user1sustainergy") {
      response = 'LOGIN_SUCCESS';
    } else if (username != "user1@sustainergy.com") {
      response = 'USERNAME_ERROR';
    } else if (password != "user1sutainergy") {
      response = 'PASSWORD_ERROR';
    } else {
      response = 'UNKNOWN_ERROR';
    }*/
    return response;
  }

  Future<void> logoutUser(BuildContext context) async {
    Provider.of<ApiService>(context).logoutUser();
    return;
  }

  Future<bool> hasToken() async {
    //read from keystore/keychain
    String value = await secureInstance.read(key: Consts.secureTokenKey);
    return value != null && value != '';
  }

  Future<FacilityResponse> getResultsFromApi(
      BuildContext context, int page) async {
    String error;
    var response = await Provider.of<ApiService>(context)
        .getAllFacilities(page)
        .catchError((onError) {
      error = onError.toString();
      showNoConnectionError(context);
    });
    if (response == null) {
      return FacilityResponse(
          count: null, items: [prefix0.Item(address: error)]);
    } else {
      if (response.statusCode == 200) {
        FacilityResponse data = FacilityResponse.fromJson(response.data);
        return data;
      } else {
        return FacilityResponse(count: null, items: [
          prefix0.Item(address: error != null ? error : response.data)
        ]);
      }
    }
  }

  void showNoConnectionError(BuildContext context) {
    ShowFlushBar(
            context: context,
            duration: 5,
            title: 'Internet connection failed',
            message: 'please try again with internet connection.',
            isInProgress: false,
            isSuccess: false,
            isThisError: true)
        .show();
  }

  Future<List<FacilityTypeModel>> getFacilityType(BuildContext context) async {
    var response = await Provider.of<ApiService>(context)
        .getFacilityTypes()
        .catchError((onError) {
      showNoConnectionError(context);
    });
    if (response != null) {
      if (response.statusCode >= 200 && response.statusCode < 400) {
        List<FacilityTypeModel> list = [];
        var data = FacilityType.fromJson(response.data);

        for (var i = 0; i < data.items.length; i++) {
          list.add(new FacilityTypeModel(
              id: data.items[i].id, name: data.items[i].name));
        }
        return list;
      } else {
        return [
          new FacilityTypeModel(
            id: null,
            name: response.data.toString().contains('socket')
                ? 'Internet connection failed'
                : 'Unauthorized',
          )
        ];
      }
    } else {
      return [
        new FacilityTypeModel(id: null, name: 'Internet connection failed')
      ];
    }
  }

  Future<List<EquipmentTypeModel>> getEquipmentType(
      BuildContext context) async {
    var response = await Provider.of<ApiService>(context)
        .getEquipmentTypes()
        .catchError((onError) {
      showNoConnectionError(context);
    });
    if (response != null) {
      if (response.statusCode >= 200 && response.statusCode < 400) {
        var data = GetEquipmentTypes.fromJson(response.data);
        List<EquipmentTypeModel> list = [];
        for (var item in data.items) {
          list.add(EquipmentTypeModel(
              id: item.id,
              name: item.name,
              requiredProperties: item.requiredProperties,
              optionalProperties: item.optionalProperties));
        }
        return list;
      } else {
        var model = EquipmentTypeModel(
          id: null,
          name: response.data.toString().contains('socket')
              ? 'Internet connection failed'
              : 'Unauthorized',
        );
        return [model];
      }
    } else {
      return [
        new EquipmentTypeModel(id: null, name: 'Internet connection failed')
      ];
    }
  }

  Future<dynamic> getEquipmentResultsFromApi(
      BuildContext context, int page) async {
    String error = '';
    var response = await Provider.of<ApiService>(context)
        .getAllEquipments(page)
        .catchError((onError) {
      error = onError.toString();
      showNoConnectionError(context);
    });

    if (response == null) {
      return EquipmentResponse(count: null, items: [prefix2.Item(name: error)]);
    } else {
      if (response.statusCode == 200) {
        EquipmentResponse data = EquipmentResponse.fromJson(response.data);
        return data;
      } else {
        return EquipmentResponse(
            count: null,
            items: [prefix2.Item(name: error != null ? error : response.data)]);
      }
    }
  }

  // POST FUNCTIONS HERE
  Future<AddFacilityResponse> addFacility(
      BuildContext context, Map<String, dynamic> model) async {
    var response = await Provider.of<ApiService>(context)
        .addFacility(model)
        .catchError((onError) {
      showNoConnectionError(context);
    });
    
     if (response != null) {
      print(response);
      if (response.statusCode == 201) {
        print(response.data);
        return AddFacilityResponse.fromJson(response.data);
      } else {
        return AddFacilityResponse(id: null, name: response.data);
      }
    } else {
      return AddFacilityResponse(
          id: null,
          name: 'We\'re facing trouble while connecting to internet...');
    }
/*    final database = Provider.of<AppDatabase>(context);
    database.addFacility(Facilitie(
      address: model['address'],
      area: model['area'],
      name: model['name'],
      postalCode: model['postalCode'],
      type: model['type'].toString(),
    ));*/
  }

  Future<AddEquipmentResponse> addEquipment(
      Map<String, dynamic> body, BuildContext context) async {
    var response = await Provider.of<ApiService>(context)
        .addEquipment(body, context)
        .catchError((onError) {
      showNoConnectionError(context);
    });
    if (response != null) {
      print(response);
      if (response.statusCode == 201) {
        print(response.data);
        return AddEquipmentResponse.fromJson(response.data);
      } else {
        return AddEquipmentResponse(id: null, name: response.data);
      }
    } else {
      return AddEquipmentResponse(
          id: null,
          name: 'We\'re facing trouble while connecting to internet...');
    }
  }

  Future<List<prefix1.Response>> addPhotosToEquipment(String photoPath,
      String namePlatePath, BuildContext context, String id) async {
    return Provider.of<ApiService>(context)
        .addPhotoToEquipment(context, File(photoPath), File(namePlatePath), id);
  }

  Future<prefix1.Response> deleteEquipment(
      String id, BuildContext context) async {
    return Provider.of<ApiService>(context).deleteEquipment(id);
  }

  Future<prefix1.Response> deleteFacility(
      String id, BuildContext context) async {
    return Provider.of<ApiService>(context).deleteFacility(id);
  }
}
